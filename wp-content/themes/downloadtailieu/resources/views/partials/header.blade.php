<div class="container-fluid header-section">
    <div class="container">
        <div class="row">
            <div class="logo col-xs-12 col-sm-12 col-md-3 col-xl-3">
                <a href="{{ home_url('/') }}">
                    @php
                        $custom_logo_id = get_theme_mod( 'custom_logo' );
                        $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                        if ( has_custom_logo() ) {
                                echo '<img src="'. esc_url( $logo[0] ) .'">';
                        } else {
                                echo '<h1>'. get_bloginfo( 'name' ) .'</h1>';
                        }
                    @endphp
                </a>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-5 col-xl-5 search">
                <div class="item-search">
                    @php
                        get_search_form();
                    @endphp
                </div>
            </div>
            <div class="login-resgiter col-xs-12 col-sm-12 col-md-4 col-xl-4 text-center">
                @php
                    if(!is_user_logged_in()){
                @endphp
                    <div class="register-login">
                        <span class="login">
                            <a href="{{ site_url('login') }}">Đăng nhập</a>
                        </span>
                        <span class="register">
                            <a href="{{ site_url('sign-up') }}">đăng ký</a>
                        </span>
                    </div>
                @php
                    }else{
                        global $current_user, $wpdb;
                        $res = $wpdb->get_row("select p.save_money, p.has_money from {$wpdb->prefix}user_extend p where p.user_id = '{$current_user->ID}'");
                @endphp
                    <div class="information row">
                        <div class="info-user col-12 col-sm-7 col-xl-7 col-md-7">
                            <p>{{ $current_user->display_name }}</p>
                            <p><a href="{{ site_url('user-dashboard') }}">(Trang cá nhân)</a></p>
                            {{-- <p>Số dư TK tiết kiệm: {{ (!empty($res->save_money) ? price_format($res->save_money) : '0.00') }} vnđ</p> --}}
                            <p>Số dư tài khoản: <span class="has_money">{{ (!empty($res->has_money) ? price_format($res->has_money) : '0.00') }}</span> vnđ</p>
                            <p><a href="{{ wp_logout_url('login') }}">Đăng xuất</a></p>
                        </div>

                        <div class="load-card col-12 col-sm-5 col-xl-5 col-md-5 text-left">
                            <button class="naptien"><a href="{{ site_url('nap-the') }}">Nạp Tiền</a></button><br>
                            <button class="downloaded-material"><a href="{{ site_url('user-dashboard/download-history/') }}">Tài liệu đã tải</a></button>
                        </div>
                    </div>
                @php
                    }
                @endphp
            </div>
        </div>
    </div>
</div>
<div class="container-fluid menu-section" style="background: #f6f6f6;">
<header class="banner">
  <div class="container">
    {{-- <a class="brand" href="{{ home_url('/') }}">
        {{ get_bloginfo('name', 'display') }}
    </a> --}}
    <nav class="nav-primary">
        @if (has_nav_menu('main-menu'))
        {{-- {!! wp_nav_menu(['theme_location' => 'main-menu', 'menu_class' => 'mean-nav']) !!} --}}
        @endif
        @php
            $params = [
                'theme_location' => 'main-menu',
                'container'       => 'ul',
                'container_class_2' => '',
                'container_class_mobile' => 'mobile-mainmenu'
            ];
            $menu = new NF\Menus\VicodersMenu('Menu1', $params);
            $menu->render();
        @endphp
    </nav>
    <div class="mobile-menu"></div>
  </div>
</header>




