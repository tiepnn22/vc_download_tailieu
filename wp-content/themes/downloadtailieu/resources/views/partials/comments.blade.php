@php
if (post_password_required()) {
  return;
}
@endphp
<section id="comments" class="comments">
    <div class="question-answer">
        @php(do_action('action_comment', get_the_ID()))
    </div>
</section>
