<footer class="content-info">
    <div class="container">
		<div class="row footer-menu">
			<div class="col-xs-6 col-sm-3 col-md-3 recomm footer_1">
				@php
					dynamic_sidebar('footer_1');
				@endphp
			</div>

			<div class="col-xs-6 col-sm-3 col-md-3 recomm footer_2">
				@php
					dynamic_sidebar('footer_2');
				@endphp
			</div>

			<div class="col-xs-6 col-sm-3 col-md-3 recomm footer_3">
				@php
					dynamic_sidebar('footer_3');
				@endphp
			</div>

			<div class="col-xs-6 col-sm-3 col-md-3 recomm footer_4">
				@php
					dynamic_sidebar('footer_4');
				@endphp
			</div>

			<div class="col-xs-6 col-sm-3 col-md-3 recomm footer_5">
				@php
					dynamic_sidebar('footer_5');
				@endphp
			</div>
		</div>
	</div>
	<div style="clear: both;"></div>
	<div class="copy-right">
		@php
			dynamic_sidebar('footer_6');
		@endphp
	</div>
</footer>
<div id="back-to-top">
    <a href="javascript:void(0)"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
</div>

@php
	if(is_active_sidebar('phone-support')) {
		dynamic_sidebar('phone-support');
	}
@endphp