@php
global $current_user;
@endphp
<article @php(post_class())>
    <div class="entry-content product_detail">
    	<div class="container">
			<div class="row">
				<div class="col-sm-9 col-md-9 col-xl-9 pad-l-0-i col-12 detail-top">
                    <div class="template-vicoders ">
                        <div class="show-message"></div>
                        <div class="detail-base row mgr-l-0 mgr-r-0">
                            <div class="col-6 col-sm-3 col-md-3 col-xl-3 text-center img-area center-block">
                                {!! do_shortcode('[icon_doc]') !!}
                                @if($check_download > 0)
                                <p class="btn-violet">đã tải</p>
                                @endif
                                <img class="avatar-item" style="background-image:url({{ $link_img }}); " src="{!! get_template_directory_uri() !!}/resources/assets/images/home/item-home.png" alt="">
                            </div>
                            <div class="col-12 col-sm-9 col-md-9 col-xl-9 pad-l-0-mobile pad-r-0-mobile info-pack">
                                <div class="col-12 title link-color pad-l-0-mobile pad-r-0-mobile">
                                    {!! $title !!}
                                </div>
                                <div class="col-12 view-down-group pad-l-0-mobile pad-r-0-mobile">
                                    <span class="view-count">
                                        <i class="fa fa-eye" aria-hidden="true"></i>&nbsp&nbsp {!! $count_view !!}
                                    </span> &nbsp &nbsp &nbsp &nbsp
                                    </span class="donwload-count">
                                        <i class="fa fa-download" aria-hidden="true"></i> &nbsp {!! $count_download !!}
                                    </span>
                                </div>
                                <div class="col-12 price-group pad-l-0-mobile pad-r-0-mobile">
                                    <label class="h4-title">Giá: </label> <span class="price text-red h4-title">{!! price_format($base_price) !!} <u>đ</u></span>
                                </div>
                                <div class="col-12 description pad-l-0-mobile pad-r-0-mobile">
                                    {!! $excerpt !!}
                                </div>  
                                <div class="col-12 social-likeshare pad-l-0-mobile pad-r-0-mobile">
                                    <div class="fb-like" data-href="[page_url]" data-layout="button" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                                </div>
                                <div class="col-12 btn-buy btn-download pad-l-0-mobile pad-r-0-mobile">
                                    <input type="hidden" name="redirect_current" id="redirect_current" value="{{ $permalink }}">
                                    {{-- @if($base_price <= 0 || $can_download === 1) --}}
                                    @if($base_price <= 0)
                                        <a href="{!! $permalink . '?wpdmdl='.$post_id !!}">
                                            <button type="button" class="btn btn-success btn-md" >
                                                <i class="fa fa-cloud-download" aria-hidden="true"></i>&nbsp Download
                                            </button>
                                        </a>
                                    @elseif( $base_price <= $has_money )
                                        <div class="wrap-btn-enough">
                                            <button type="button" class="btn btn-success btn-md btn-show-confirm" data-toggle="modal" data-target="#enoughmoney">
                                                <i class="fa fa-cloud-download" aria-hidden="true"></i>&nbsp Download
                                            </button>
                                        </div>
                                        <div class="modal fade" id="enoughmoney" tabindex="-1" role="dialog" aria-labelledby="enoughmoneyLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-body text-center ">
                                                        <div class="notify-text uppercase">Tài khoản nạp thẻ của bạn sẽ bị trừ <div class="text-red"> {{ price_format($base_price) }} VNĐ</div></div>
                                                        <div class="text-center button-check">
                                                            <input type="hidden" id="inp_uid" nam="uid" value="{{ $current_user->ID }}">
                                                            <input type="hidden" id="inp_pid" name="pid" value="{{ $post_id }}">
                                                            <button type="button" data-dismiss="modal" class="btn btn-success btn-confirm-payment">Xác nhận</button>
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Hủy</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <button type="button" class="btn btn-success btn-md" data-toggle="modal" data-target="#poormoney">
                                            <i class="fa fa-cloud-download" aria-hidden="true"></i>&nbsp Download
                                        </button>
                                        <div class="modal fade" id="poormoney" tabindex="-1" role="dialog" aria-labelledby="poormoneyLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-body">
                                                        <div class="text-red text-center notify-text uppercase">Tài khoản nạp thẻ của bạn không đủ để thanh toán.</div>
                                                        <div class="text-center button-check">
                                                            <button type="button" class="btn btn-success"><a class="text-white" href="{!! site_url('nap-the') !!}"> Nạp thêm </a></button>
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Hủy</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="detail-extend">
                            <div class="col-12 doc-preview pad-l-0-i pad-r-0-i">
                                {!! do_shortcode($description) !!}
                                <div class="layout"></div>
                            </div>
                        </div>
                    </div>
        			<div class="row package-section">
        				@php
                            if(is_active_sidebar('block-package')):
                                dynamic_sidebar('block-package');
                            endif;
                        @endphp
        			</div>
    				@include('partials.comments')
				</div>
				<div class="col-12 col-sm-3 col-md-3 col-xl-3 sidebar-section pad-l-0-i pad-r-0-i">
					@php
                        get_sidebar();
                    @endphp
                    {{-- @include('components.sidebar-wpdmpro') --}}
				</div>
			</div>
		</div>
    </div>
</article>
