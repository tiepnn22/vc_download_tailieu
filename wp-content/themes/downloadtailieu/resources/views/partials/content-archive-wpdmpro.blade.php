<div class="list-tailieu">
	<div class="content-list-tailieu">
		@if($child_terms)
			@foreach($child_terms as $key => $child)
				<div class="title-categori">
					<p class="title">{{ $child->name }}</p>
				</div>
				@php
					$sub_child_terms = getChildTerms($child->term_id);
					if(!empty($sub_child_terms)) {
						echo '<div class="row folder-tailieu">';
							foreach ($sub_child_terms as $sub_val) {
									$count = 0;
									$name_parent_cat = $sub_val->name;
									$url_parent_cat = get_category_link($sub_val->term_id);
									$count += count_nested_categories($sub_val->term_id);
								@endphp
								<div class="col-12 col-md-3 col-sm-6 col-xl-3 list-folder">
									<div class="parent_cat">
										<div class="images_parent_cat">
											<a href="{{ $url_parent_cat }}">
												<img class="img-responsive center-block" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/tai-lieu/folder-tailieu.png" alt="">
											</a>
										</div>

										<div class="name_parent_cat">
											<a href="{{ $url_parent_cat }}">
												<p class="title-item fw-600">{{ $name_parent_cat }}</p>
												<p class="count_post">({{ $count }} tài liệu)</p>
											</a>
										</div>
									</div>
								</div>
								@php
							}
						echo '</div>';
					} else {
						$count = 0;
						$name_parent_cat = $child->name;
						$url_parent_cat = get_category_link($child->term_id);
						$count += count_nested_categories($child->term_id);
						echo '<div class="row folder-tailieu">';
							@endphp
							<div class="col-xs-12 col-md-3 col-sm-6 list-folder">
								<div class="parent_cat">
									<div class="images_parent_cat">
										<a href="{{ $url_parent_cat }}">
											<img class="img-responsive" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/tai-lieu/folder-tailieu.png" alt="">
										</a>
									</div>

									<div class="name_parent_cat">
										<a href="{{ $url_parent_cat }}">
											<p class="title-item fw-600">{{ $name_parent_cat }}</p>
											<p class="count_post">({{ $count }} tài liệu)</p>
										</a>
									</div>
								</div>
							</div>
							@php
						echo '</div>';
					}
				@endphp
			@endforeach
		@else
			<div class="title-categori">
				<p class="title">{{ $current_term->name }}</p>
			</div>

			<div class="row list-item">
				@php
					if(!empty($get_post_categoris->posts)) :
						foreach($get_post_categoris->posts as $key => $val) {
							$link = get_permalink($val->ID);
							@endphp
							<div class="col-xs-12 col-md-3 col-sm-6 recomm-homepage">
								<div class="images-item">
									<a href="{{ $link }}">
										<img class="icon-topic" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/home/icon-work.png" alt="">
										@if($val->check_download > 0)
		                                	<p class="btn-violet">Đã tải</p>
		                                @endif
										<img class="avatar-item" style="background-image:url({{  wp_get_attachment_url(get_post_thumbnail_id($val->ID)) }}); " src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/home/item-home.png" alt="">

									</a>
								</div>
								<div class="info-item">
									<a href="{{ $link }}">
										<p class="title-item fw-600">{!! $val->post_title !!}</p>
									</a>
								</div>
							</div>
							@php
						}
					else:
						echo '<div class="col-xs-12 col-md-3 col-sm-6 recomm-homepage fw-600">';
						echo 'Không có dữ liệu';
						echo '</div>';
					endif;
				@endphp
			</div>
		@endif
	</div>
</div>
