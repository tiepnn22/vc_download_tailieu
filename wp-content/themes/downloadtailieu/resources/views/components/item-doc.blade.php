{!! do_shortcode('[icon_doc]') !!}
@if($check_download > 0)
<p class="btn-violet">đã tải</p>
@endif
@if(!empty($link_doc))
	<a href="{{ $link_doc }}"><img class="avatar-item center-block" style="background-image:url({{ $link_img }}); " src="{!! get_template_directory_uri() !!}/resources/assets/images/home/item-home.png" alt=""></a>
@else 
	<img class="avatar-item" style="background-image:url({{ $link_img }}); " src="{!! get_template_directory_uri() !!}/resources/assets/images/home/item-home.png" alt="">
@endif