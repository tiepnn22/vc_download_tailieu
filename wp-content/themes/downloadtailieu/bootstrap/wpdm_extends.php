<?php

use App\Models\Charge;
use App\Models\Transaction;
use App\Models\UserExtend;
use NF\Payment\Baokim;
use NF\Payment\NganLuong;

/**
 *
 */
class Wpdm_Extend
{
    public function __construct()
    {
        add_filter('wpdm_user_dashboard_menu', [$this, 'addTransactionDashboardMenu']);
        add_filter('wpdm_user_dashboard_menu', [$this, 'removePurcharseDashboardMenu']);
        add_action('user_register', [$this, 'register_success'], 10, 1);

        add_action('wp_ajax_baokim_charge_card', [$this, 'baokim_charge_card']);
        add_action('wp_ajax_nopriv_baokim_charge_card', [$this, 'baokim_charge_card']);

        add_action('wp_ajax_nganluong_charge_card', [$this, 'nganluong_charge_card']);
        add_action('wp_ajax_nopriv_nganluong_charge_card', [$this, 'nganluong_charge_card']);

        add_action('wp_ajax_create_transaction', [$this, 'createTransaction']);
        add_action('wp_ajax_nopriv_create_transaction', [$this, 'createTransaction']);

        add_action('save_post', [$this, 'updatePostMetaExtendInfo'], 10, 3);
    }

    public function addTransactionDashboardMenu($menu)
    {
        $menu = array_merge(array_splice($menu, 0, 2), ['transaction' => ['name' => __('Lịch sử giao dịch', 'wpdm-premium-package'), 'callback' => [$this, 'wpdmpp_transaction_items']]], $menu);
        return $menu;
    }

    public function removePurcharseDashboardMenu($menu)
    {
        unset($menu['purchases']);
        return $menu;
    }

    public function wpdmpp_transaction_items()
    {
        ob_start();
        include_once wpdm_tpl_path('user-dashboard/transaction-history.php');
        return ob_get_clean();
    }

    public function register_success($user_id)
    {
        // $user_meta=get_userdata($user_id);
        // $user_roles=$user_meta->roles;

        // if ( in_array( 'subscriber', (array) $user_roles ) ) {
        $check_charge = Charge::where('user_id', $user_id)->where('type', 2)->first();
        if (!$check_charge) {
            $charge              = new Charge();
            $charge->user_id     = $user_id;
            $charge->seri_card   = '';
            $charge->number_card = '';
            $charge->amount      = 10000;
            $charge->reason      = 'Đăng ký tài khoản mới';
            $charge->type        = 2;
            $charge->type_card   = '';
            $charge->status      = 1;
            $result              = $charge->save();
        } else {
            $result = true;
        }
        if ($result) {
            $user_extend = UserExtend::where('user_id', $user_id)->first();
            if ($user_extend) {
                $user_extend->has_money = (float) $user_extend->has_money + 10000;
                $user_extend->save();
            } else {
                $new_extend             = new UserExtend();
                $new_extend->user_id    = $user_id;
                $new_extend->has_money = 10000;
                $new_extend->save();
            }
        } else {
            new WP_Error('406', __("Không thể lưu được thông tin", "downloadtailieu"));
        }
        // }
    }

    public function baokim_charge_card()
    {
        global $current_user, $wpdb;

        $params = $_REQUEST;

        $homenetwork = 'VIETEL';
        switch ($params['inp_homenetwork']) {
            case 'viettel':
                $homenetwork = 'VIETEL';
                break;
            case 'vinaphone':
                $homenetwork = 'VINA';
                break;
            case 'mobiphone':
                $homenetwork = 'MOBI';
                break;
            default:
                $homenetwork = 'VIETEL';
                break;
        }

        $params_std = [
            'mang' => $homenetwork,
            'pin'  => $params['txtpin'],
            'seri' => $params['txtseri'],
        ];

        $baokim   = new Baokim($params_std);
        $response = $baokim->sendRequest();

        $handle_response = json_decode($response, true);
        if ($handle_response['status'] == 200) {
            $charge              = new Charge();
            $charge->user_id     = $current_user->ID;
            $charge->seri_card   = $params['txtseri'];
            $charge->number_card = $params['txtpin'];
            $charge->amount      = $handle_response['status']['amount'];
            $charge->reason      = 'Nạp mới';
            $charge->type        = 1;
            $charge->type_card   = $homenetwork;
            $charge->status      = 1;
            $result              = $charge->save();
            $user_ex             = UserExtend::where('user_id', $current_user->ID)->first();
            $user_ex->has_money += $handle_response['status']['amount'];
            $user_ex->save();
            $data = [
                'status' => $handle_response['status'],
                'result' => [
                    'message' => 'Nạp thẻ thành công',
                ],
            ];
        } else {
            $data = [
                'status' => $handle_response['status'],
                'result' => [
                    'message' => 'Nạp thẻ không thành công. Hãy kiểm tra lại các thông tin.',
                ],
            ];
        }

        echo json_encode($data);die;
    }

    public function nganluong_charge_card()
    {
        global $current_user, $wpdb;

        $params = $_REQUEST;

        $homenetwork = 'VIETTEL';
        switch ($params['inp_homenetwork']) {
            case 'viettel':
                $homenetwork = 'VIETTEL';
                break;
            case 'vinaphone':
                $homenetwork = 'VNP';
                break;
            case 'mobiphone':
                $homenetwork = 'VMS';
                break;
            default:
                $homenetwork = 'VIETTEL';
                break;
        }

        $user_ex = UserExtend::where('user_id', $current_user->ID)->first();

        $params_std = [
            'mang'     => $homenetwork,
            'pin'      => $params['txtpin'],
            'seri'     => $params['txtseri'],
            'fullname' => $current_user->display_name,
            'email'    => $current_user->user_email,
            'phone'    => $user_ex->phone,
            'order_id' => uniqid(),
        ];

        // echo json_encode($params_std); die;

        $nganluong = new NganLuong($params_std);
        $response  = $nganluong->sendRequest();

        // echo $response;die;
        $handle_response = json_decode($response, true);
        if ($handle_response['status'] == 200) {
            $arr_result = explode("|", $handle_response['result']);
            if (count($arr_result) == 13 && ($arr_result[0] == 00 || $arr_result[0] == '00')) {
                $charge                 = new Charge();
                $charge->user_id        = $current_user->ID;
                $charge->transaction_id = $arr_result[12];
                $charge->seri_card      = $arr_result[4];
                $charge->number_card    = $arr_result[3];
                $charge->amount         = $arr_result[10];
                $charge->reason         = 'Nạp mới';
                $charge->type           = 1;
                $charge->type_card      = $arr_result[5];
                $charge->status         = 1;
                $result                 = $charge->save();

                if ($arr_result[10] == 500 || $arr_result[10] == '500') {
                    $user_ex->has_money += ($arr_result[10] * 2);
                } else {
                    $user_ex->has_money += $arr_result[10];
                }
                $user_ex->save();
                $message = $this->GetErrorMessage($arr_result[0]);
                $status  = 200;
            } else {
                $message = $this->GetErrorMessage($arr_result[0]);
                $status  = $arr_result[0];
            }
            $data = [
                'status' => $status,
                'result' => [
                    'message' => $message,
                ],
            ];
        } else {
            $data = [
                'status' => $handle_response['status'],
                'result' => [
                    'message' => $handle_response['error'],
                ],
            ];
        }

        echo json_encode($data);die;
    }

    public function createTransaction()
    {
        global $current_user;
        $params = $_REQUEST;
        if (empty($params)) {
            echo json_encode(['code' => 406, 'message' => 'Không nhận được dữ liệu']);
            exit;
        }
        if ($params['uid'] != $current_user->ID) {
            echo json_encode(['code' => 406, 'message' => 'Không nhận được dữ liệu']);
            exit;
        }

        if (function_exists('get_package_data')) {
            $base_price = get_package_data($params['pid'], 'base_price');
        } else {
            new WP_Error(404, __("Không thể thực hiện chức năng này", "downloadtailieu"));
            exit;
        }
        $user_ex = UserExtend::where('user_id', $current_user->ID)->first();
        if ($user_ex) {
            $user_ex->has_money = (float) $user_ex->has_money - (float) $base_price;
            $user_ex->save();
            $transaction_id = ($current_user->ID) . '_' . ($params['pid']) . '_' . (time());
            $link_dl        = $params['redirect_current'] . '?wpdmdl=' . $params['pid'];
            $html_link_dl   = '<a href="' . ($link_dl) . '">tại đây</a>';
            $this->saveTransactionToDatabase($current_user->ID, $params['pid'], $transaction_id, 1);
            $log = new \WPDM_Stats();
            $log->NewStat($params['pid'], $current_user->ID, '');
            $data = [
                $params['pid'] => [
                    'id'         => $params['pid'],
                    'user_id'    => $current_user->ID,
                    'base_price' => $base_price,
                ],
            ];
            $items = serialize(array_keys($data));
            $order = new \Order();
            $order->NewOrder(uniqid(), mb_convert_encoding(get_the_title($params['pid']), 'HTML-ENTITIES', "UTF-8"), $items, $base_price, $current_user->ID, 'completed', 'completed', '', '', 'baokim');
            $order->Update(['download' => 1], $_SESSION['orderid']);
            echo json_encode([
                'code'    => 200,
                'message' => '<strong>Thanh toán thành công </strong>. Bạn có thể tải xuống <u>' . $html_link_dl . '</u>',
                'btn_dl'  => '<a href="' . ($link_dl) . '"><button type="button" class="btn btn-success btn-md btn-show-confirm"><i class="fa fa-cloud-download" aria-hidden="true"></i>&nbsp Download</button></a>',
                'has_money' => price_format($user_ex->has_money)
            ]);
            exit;
        } else {
            echo json_encode(['code' => 406, 'message' => 'Không nhận được dữ liệu']);
            exit;
        }
    }

    public function GetErrorMessage($error_code)
    {
        $arrCode = [
            '00' => 'Giao dịch thành công',
            '99' => 'Lỗi, tuy nhiên lỗi chưa được định nghĩa hoặc chưa xác định được nguyên nhân',
            '01' => 'Lỗi, địa chỉ IP truy cập API của NgânLượng.vn bị từ chối',
            '02' => 'Lỗi, tham số gửi từ merchant tới NgânLượng.vn chưa chính xác (thường sai tên tham số hoặc thiếu tham số)',
            '03' => 'Lỗi, Mã merchant không tồn tại hoặc merchant đang bị khóa kết nối tới NgânLượng.vn',
            '04' => 'Lỗi, Mã checksum không chính xác (lỗi này thường xảy ra khi mật khẩu giao tiếp giữa merchant và NgânLượng.vn không chính xác, hoặc cách sắp xếp các tham số trong biến params không đúng)',
            '05' => 'Tài khoản nhận tiền nạp của merchant không tồn tại',
            '06' => 'Tài khoản nhận tiền nạp của merchant đang bị khóa hoặc bị phong tỏa, không thể thực hiện được giao dịch nạp tiền',
            '07' => 'Thẻ đã được sử dụng ',
            '08' => 'Thẻ bị khóa',
            '09' => 'Thẻ hết hạn sử dụng',
            '10' => 'Thẻ chưa được kích hoạt hoặc không tồn tại',
            '11' => 'Mã thẻ sai định dạng',
            '12' => 'Sai số serial của thẻ',
            '13' => 'Mã thẻ và số serial không khớp',
            '14' => 'Thẻ không tồn tại',
            '15' => 'Thẻ không sử dụng được',
            '16' => 'Số lần thử (nhập sai liên tiếp) của thẻ vượt quá giới hạn cho phép',
            '17' => 'Hệ thống Telco bị lỗi hoặc quá tải, thẻ chưa bị trừ',
            '18' => 'Hệ thống Telco bị lỗi hoặc quá tải, thẻ có thể bị trừ, cần phối hợp với NgânLượng.vn để tra soát',
            '19' => 'Kết nối từ NgânLượng.vn tới hệ thống Telco bị lỗi, thẻ chưa bị trừ (thường do lỗi kết nối giữa NgânLượng.vn với Telco, ví dụ sai tham số kết nối, mà không liên quan đến merchant)',
            '20' => 'Kết nối tới telco thành công, thẻ bị trừ nhưng chưa cộng tiền trên NgânLượng.vn'];

        return $arrCode[$error_code];
    }

    public function saveTransactionToDatabase($user_id, $doc_id, $transaction_id, $status)
    {
        $trans           = new Transaction();
        $trans->user_id  = $user_id;
        $trans->doc_id   = $doc_id;
        $trans->trans_id = $transaction_id;
        $trans->status   = $status;
        $trans->save();
    }

    public function updatePostMetaExtendInfo($post_id, $post, $update)
    {
        $post_type = get_post_type($post_id);
        if ("wpdmpro" != $post_type) {
            return;
        }
        if (isset($_POST['source-upload'])) {
            update_post_meta($post_id, '__vicoders_source_upload', $_POST['source-upload']);
        }
        if (isset($_POST['typedoc-upload'])) {
            update_post_meta($post_id, '__vicoders_typedoc_upload', $_POST['typedoc-upload']);
        }
        if (isset($_POST['docs-grade'])) {
            update_post_meta($post_id, '__vicoders_docs_grade', $_POST['docs-grade']);
        }
    }
}

$wpdm_extend = new Wpdm_Extend();
