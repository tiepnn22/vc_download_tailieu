<?php

global $wpdb;
define('PREFIX_TABLE', $wpdb->prefix);

require dirname(__DIR__) . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
require __DIR__ . DIRECTORY_SEPARATOR . 'load.php';
require __DIR__ . DIRECTORY_SEPARATOR . 'setting.php';
require __DIR__ . DIRECTORY_SEPARATOR . 'wpdm_extends.php';
require __DIR__ . DIRECTORY_SEPARATOR . 'helper_nganluong.php';

$app = new \NF\Foundation\Application(dirname(__DIR__));

return $app;
