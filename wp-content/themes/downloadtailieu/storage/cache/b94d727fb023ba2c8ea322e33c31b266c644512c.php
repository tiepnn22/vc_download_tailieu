<?php $__env->startSection('content'); ?>
<div class="payment-section">
    <div class="container">
        <div class="row info-payment">
            <div class="col-12 col-sm-6 col-md-6 col-xl-6">
                <div class="text-red">
                    Nạp tiền bằng thẻ điện thoại
                </div>
                <form class="form-horizontal form-bk" role="form" method="post" action="">
                    <div class="form-group">
                        <label class="control-label">* Loại thẻ ( Tích vào ô chọn loại thẻ Viettel, Vinaphone, Mobiphone)</label>
                        <div class="row pad-l-0-i">
                            <div class="viettel-block col-2 text-center">
                                <img class="icon-topic" src="<?php echo get_stylesheet_directory_uri() . '/resources/assets/images/payments/viettel.png'; ?>" alt="">
                                <p><input type="radio" name="inp_homenetwork" value="viettel"></p>
                            </div>
                            <div class="vina-block col-2 push-sm-2 text-center">
                                <img class="icon-topic" src="<?php echo get_stylesheet_directory_uri() . '/resources/assets/images/payments/vinaphone.png'; ?>" alt="">
                                <p><input type="radio" name="inp_homenetwork" value="vinaphone"></p>
                            </div>
                            <div class="mobi-block col-2 push-sm-2 text-center">
                                <img class="icon-topic" src="<?php echo get_stylesheet_directory_uri() . '/resources/assets/images/payments/mobiphone.png'; ?>" alt="">
                                <p><input type="radio" name="inp_homenetwork" value="mobiphone"></p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-tooltip">
                        <label for="txtpin" class="control-label">* Mã thẻ</label>
                        <div class="">
                            <input type="text" class="form-control" id="txtpin" name="txtpin" placeholder="Điển mã thẻ viết liền (0 cách trống, 0 gạch ngang)" data-toggle="tooltip" data-title="Mã số sau lớp bạc mỏng" />
                        </div>
                    </div>
                    <div class="form-group form-tooltip">
                        <label for="txtseri" class="control-label">* Số seri</label>
                        <div class="">
                            <input type="text" class="form-control" id="txtseri" name="txtseri" placeholder="Điển số seri viết liền (0 cách trống, 0 gạch ngang)" data-toggle="tooltip" data-title="Mã seri nằm sau thẻ">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="">
                            <button type="submit" class="btn btn-primary" name="napthe" id="submit-baokim">Nạp thẻ ngay</button>
                        </div>
                    </div>
                </form>    
            </div>
            <div class="col-12 col-sm-6 col-md-6 col-xl-6">
                <div class="col-12 link-history">
                    <div class="link-history-text">Số dư: <span>0,000đ</span> (Lịch sử giao dịch)</div>
                </div>
                <div class="col-12 value-payment">
                    <table class="table table-bordered">
                        <tr>
                            <th>Mệnh giá thẻ cào</th>
                            <th>Tiền nhận được</th>
                        </tr>
                        <tr>
                            <td>Nạp thẻ mệnh giá 10.000 vnđ</td> 
                            <td>Nhận 10.000 đ vào tài khoản</td> 
                        </tr>
                        <tr>
                            <td>Nạp thẻ mệnh giá 10.000 vnđ</td> 
                            <td>Nhận 10.000 đ vào tài khoản</td> 
                        </tr>
                        <tr>
                            <td>Nạp thẻ mệnh giá 10.000 vnđ</td> 
                            <td>Nhận 10.000 đ vào tài khoản</td> 
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <div class="row info-payment-ask">
            <p style="color: red;">Tại sao phải chọn GÓI TIẾT KIỆM</p>

            - Bạn sẽ tiết kiệm được chi phí tối đa khi tải tài liệu trên website
            - Nếu bạn nạp thẻ 100.000 bạn chỉ được 100.000 vào tài khoản. Nhưng khi bạn đăng ký các GÓI TIẾT KIỆM bạn sẽ được nhân lên rất nhiều lần số tiền ban đầu bạn bỏ ra khi đăng ký GÓI TIẾT KIỆM.
            - Gói càng cao số tiền được nhận trong tài khoản càng nhiều điều đó có nghĩa bạn tải đề thi, tài liệu càng rẻ và được càng được càng nhiều ưu đãi khác.
            - Nhằm giữ giá trị tài liệu chúng tôi sẽ giới hạn lượt đăng ký các GÓI TIẾT KIỆM theo tháng. Nếu hết lượt đăng ký bạn sẽ phải để sang tháng đăng ký hoặc chấp nhận nạp thẻ mua tài liệu với giá cao.

            <img style="margin: 30px auto;" src="<?php echo get_stylesheet_directory_uri() . '/resources/assets/images/payments/payment-ask.png'; ?>" alt="">

            Vui lòng thanh toán qua 1 trong 6 ngân hàng ở phía dưới để đăng ký
            - Chuyển khoản tại ngân hàng gần nhất
            - Chuyển khoản tại cây ATM gần nhất (khuyến khích)
            - Chuyển khoản bằng điện thoại, internetbanking (khuyến khích)
            - Thanh toán bằng nạp thẻ cào với mệnh giá tương ứng giá gói
        </div>

    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>