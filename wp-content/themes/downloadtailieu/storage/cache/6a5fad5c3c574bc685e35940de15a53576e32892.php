<?php 
global $current_user;
 ?>
<article <?php (post_class()); ?>>
    <div class="entry-content product_detail">
    	<div class="container">
			<div class="row">
				<div class="col-sm-9 col-md-9 col-xl-9 pad-l-0-i col-12 detail-top">
                    <div class="template-vicoders ">
                        <div class="show-message"></div>
                        <div class="detail-base row mgr-l-0 mgr-r-0">
                            <div class="col-6 col-sm-3 col-md-3 col-xl-3 text-center img-area center-block">
                                <?php echo do_shortcode('[icon_doc]'); ?>

                                <?php if($check_download > 0): ?>
                                <p class="btn-violet">đã tải</p>
                                <?php endif; ?>
                                <img class="avatar-item" style="background-image:url(<?php echo e($link_img); ?>); " src="<?php echo get_template_directory_uri(); ?>/resources/assets/images/home/item-home.png" alt="">
                            </div>
                            <div class="col-12 col-sm-9 col-md-9 col-xl-9 pad-l-0-mobile pad-r-0-mobile info-pack">
                                <div class="col-12 title link-color pad-l-0-mobile pad-r-0-mobile">
                                    <?php echo $title; ?>

                                </div>
                                <div class="col-12 view-down-group pad-l-0-mobile pad-r-0-mobile">
                                    <span class="view-count">
                                        <i class="fa fa-eye" aria-hidden="true"></i>&nbsp&nbsp <?php echo $count_view; ?>

                                    </span> &nbsp &nbsp &nbsp &nbsp
                                    </span class="donwload-count">
                                        <i class="fa fa-download" aria-hidden="true"></i> &nbsp <?php echo $count_download; ?>

                                    </span>
                                </div>
                                <div class="col-12 price-group pad-l-0-mobile pad-r-0-mobile">
                                    <label class="h4-title">Giá: </label> <span class="price text-red h4-title"><?php echo price_format($base_price); ?> <u>đ</u></span>
                                </div>
                                <div class="col-12 description pad-l-0-mobile pad-r-0-mobile">
                                    <?php echo $excerpt; ?>

                                </div>  
                                <div class="col-12 social-likeshare pad-l-0-mobile pad-r-0-mobile">
                                    <div class="fb-like" data-href="[page_url]" data-layout="button" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                                </div>
                                <div class="col-12 btn-buy btn-download pad-l-0-mobile pad-r-0-mobile">
                                    <input type="hidden" name="redirect_current" id="redirect_current" value="<?php echo e($permalink); ?>">
                                    
                                    <?php if($base_price <= 0): ?>
                                        <a href="<?php echo $permalink . '?wpdmdl='.$post_id; ?>">
                                            <button type="button" class="btn btn-success btn-md" >
                                                <i class="fa fa-cloud-download" aria-hidden="true"></i>&nbsp Download
                                            </button>
                                        </a>
                                    <?php elseif( $base_price <= $has_money ): ?>
                                        <div class="wrap-btn-enough">
                                            <button type="button" class="btn btn-success btn-md btn-show-confirm" data-toggle="modal" data-target="#enoughmoney">
                                                <i class="fa fa-cloud-download" aria-hidden="true"></i>&nbsp Download
                                            </button>
                                        </div>
                                        <div class="modal fade" id="enoughmoney" tabindex="-1" role="dialog" aria-labelledby="enoughmoneyLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-body text-center ">
                                                        <div class="notify-text uppercase">Tài khoản nạp thẻ của bạn sẽ bị trừ <div class="text-red"> <?php echo e(price_format($base_price)); ?> VNĐ</div></div>
                                                        <div class="text-center button-check">
                                                            <input type="hidden" id="inp_uid" nam="uid" value="<?php echo e($current_user->ID); ?>">
                                                            <input type="hidden" id="inp_pid" name="pid" value="<?php echo e($post_id); ?>">
                                                            <button type="button" data-dismiss="modal" class="btn btn-success btn-confirm-payment">Xác nhận</button>
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Hủy</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php else: ?>
                                        <button type="button" class="btn btn-success btn-md" data-toggle="modal" data-target="#poormoney">
                                            <i class="fa fa-cloud-download" aria-hidden="true"></i>&nbsp Download
                                        </button>
                                        <div class="modal fade" id="poormoney" tabindex="-1" role="dialog" aria-labelledby="poormoneyLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-body">
                                                        <div class="text-red text-center notify-text uppercase">Tài khoản nạp thẻ của bạn không đủ để thanh toán.</div>
                                                        <div class="text-center button-check">
                                                            <button type="button" class="btn btn-success"><a class="text-white" href="<?php echo site_url('nap-the'); ?>"> Nạp thêm </a></button>
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Hủy</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="detail-extend">
                            <div class="col-12 doc-preview pad-l-0-i pad-r-0-i">
                                <?php echo do_shortcode($description); ?>

                                <div class="layout"></div>
                            </div>
                        </div>
                    </div>
        			<div class="row package-section">
        				<?php 
                            if(is_active_sidebar('block-package')):
                                dynamic_sidebar('block-package');
                            endif;
                         ?>
        			</div>
    				<?php echo $__env->make('partials.comments', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				</div>
				<div class="col-12 col-sm-3 col-md-3 col-xl-3 sidebar-section pad-l-0-i pad-r-0-i">
					<?php 
                        get_sidebar();
                     ?>
                    
				</div>
			</div>
		</div>
    </div>
</article>
