<div class="sidebar-section">
	<div class="title">
		Tài liệu liên quan
	</div>
	<div class="relation-file">
		<?php if(!empty($relate_docs)): ?>
			<?php $__currentLoopData = $relate_docs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php 
					$images = wp_get_attachment_url(get_post_thumbnail_id($item->ID));
				 ?>
				<div class="item row">
					<div class="col-12 col-sm-3 col-md-3 col-xl-3 img-area">
						<img class="icon-topic" src="<?php echo get_stylesheet_directory_uri() . '/resources/assets/images/home/icon-work.png'; ?>" alt="">
						<img class="avatar-item" style="background-image:url(<?php echo e($images); ?>); " src="<?php echo get_stylesheet_directory_uri() . '/resources/assets/images/home/item-home.png'; ?>" alt="">
					</div>
					<div class="col-12 col-sm-9 col-md-9 col-xl-9 text-left title-package">
						<a href="<?php echo get_permalink($item->ID); ?>"><?php echo e($item->post_title); ?></a>
						<div class="view-down-group">
							<span class="view-count">
								<i class="fa fa-eye" aria-hidden="true"></i>&nbsp <?php echo get_package_data($item->ID, 'view_count'); ?>

							</span> &nbsp &nbsp
							</span class="donwload-count">
								<?php 
									$download_count = get_package_data($item->ID, 'download_count');
									if($download_count == 0){
										$download_count = 0;
									}
								 ?>
								<i class="fa fa-download" aria-hidden="true"></i> &nbsp <?php echo $download_count; ?>


							</span>
						</div>
					</div>
				</div>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		<?php endif; ?>
	</div>
</div>