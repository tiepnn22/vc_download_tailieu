<?php 
	$unique_id = esc_attr(uniqid('search-form-'));
 ?>
<div class="center-block col-12 col-sm-12 col-md-10 col-xl-10">
	<form role="search" method="get" class="search-form" action="<?php echo esc_url(home_url('/')); ?>">
		<div class="input-group">
	        <input type="search" id="<?php echo $unique_id; ?>" value="<?php echo get_search_query(); ?>" name="s" class="form-control search-field" placeholder="Tìm kiếm tài liệu ...">
	        <span class="input-group-btn input-group-custom">
	            <button type="submit" class="search-submit"><i class="fa fa-search" aria-hidden="true"></i></button>
	        </span>
	    </div>
	</form>
</div>