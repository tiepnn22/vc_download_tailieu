<div class="list-tailieu">
<!-- 	<div class="breadcrumb">
		trang chủ -> list tài liệu
	</div> -->
	<?php 
		$object_current = get_queried_object();
	 ?>
	<div class="content-list-tailieu">
		<div class="title-categori">
			<p class="title"><?php echo e($object_current->name); ?></p>
		</div>

		<div class="row list-item">
			<?php 
				$paged = ($_GET['trang'] >= 2) ? $_GET['trang'] : 1;
				$post_cat = [
					'post_type'=>'wpdmpro',
                    'posts_per_page' => -1,
                    'post_status'    => 'publish',
                    'tax_query'     => array(
				        array(
				            'taxonomy'          => 'wpdmcategory',
				            'terms'             => array($object_current->term_id),
				            'field'             => 'term_id',
				            'operator'          => 'AND',
				            'include_children'  => false
				        )
				    ),
                    'paged' => $paged,
				];

				$get_post_categoris = new WP_Query($post_cat);

				if($get_post_categoris->have_posts()) :

					foreach($get_post_categoris->posts as $key => $val) {

					$link = get_permalink($val->ID);
			 ?>


				<div class="col-xs-12 col-md-3 col-sm-6 recomm-homepage">
					<div class="images-item">
						<a href="<?php echo e($link); ?>">
							<img class="icon-topic" src="<?php echo e(get_stylesheet_directory_uri()); ?>/resources/assets/images/home/icon-work.png" alt="">

							<img class="avatar-item" style="background-image:url(<?php echo e(wp_get_attachment_url(get_post_thumbnail_id($val->ID))); ?>); " src="<?php echo e(get_stylesheet_directory_uri()); ?>/resources/assets/images/home/item-home.png" alt="">

						</a>
					</div>
					<div class="info-item">
						<a href="#">
							<p class="title-item"><?php echo $val->post_title; ?></p>
						</a>
					</div>
				</div>

			<?php 
					}
				endif;
			 ?>
		</div>
			<div class="paginate">
	            <?php 
	            	
		            $total_pages = $get_post_categoris->max_num_pages;

		            if ($total_pages > 1) :

		                $current_page = max(1, $paged);

		                echo paginate_links(array(
					        'base' => @add_query_arg('trang','%#%'),
					        'format' => '?trang=%#%',
					        'current' => $current_page,
					        'total' => $total_pages,
		                    'prev_text'    => __('<'),
		                    'next_text'    => __('>')
					    ));
	             ?>
	            <?php 
	            	endif;
	             ?>
	            <?php  wp_reset_postdata();  ?>
	        </div>
	</div>

	<div class="row folder-tailieu">
		<?php 
			$terms = get_terms( array(
			    'taxonomy' => 'wpdmcategory',
			    'hide_empty' => false,
			) );
			foreach ($terms as $value) {

				$name_parent_cat = $value->name;

				$url_parent_cat = get_category_link($value->term_id);

				$count = $value->count;

				if($object_current->term_id == $value->parent && $count > 0){
		 ?>
				<div class="col-xs-12 col-md-3 col-sm-3 list-folder">
					<div class="parent_cat">
						<div class="images_parent_cat">
							<a href="<?php echo e($url_parent_cat); ?>">
								<img class="img-responsive" src="<?php echo e(get_stylesheet_directory_uri()); ?>/resources/assets/images/tai-lieu/folder-tailieu.png" alt="">
							</a>
						</div>

						<div class="name_parent_cat">
							<a href="<?php echo e($url_parent_cat); ?>">
								<p><?php echo e($name_parent_cat); ?></p>
								<p class="count_post">(<?php echo e($count); ?> tài liệu)</p>
							</a>
						</div>
					</div>
				</div>
		<?php 
				}
			}
		 ?>
	</div>
</div>