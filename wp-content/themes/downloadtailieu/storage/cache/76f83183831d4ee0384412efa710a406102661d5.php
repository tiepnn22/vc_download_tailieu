<article <?php (post_class()); ?>>
    <div class="detail-post white-block">
        <header>
            <h3 class="entry-title"><?php echo e(get_the_title()); ?></h3>
        </header>
        <div class="entry-content">
            <?php 
                the_content();
             ?>
        </div>
    </div>
    
</article>