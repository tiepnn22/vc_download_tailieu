<footer class="content-info">
    <div class="container">
		<div class="row footer-menu">
			<div class="col-xs-6 col-sm-3 col-md-3 recomm footer_1">
				<?php 
					dynamic_sidebar('footer_1');
				 ?>
			</div>

			<div class="col-xs-6 col-sm-3 col-md-3 recomm footer_2">
				<?php 
					dynamic_sidebar('footer_2');
				 ?>
			</div>

			<div class="col-xs-6 col-sm-3 col-md-3 recomm footer_3">
				<?php 
					dynamic_sidebar('footer_3');
				 ?>
			</div>

			<div class="col-xs-6 col-sm-3 col-md-3 recomm footer_4">
				<?php 
					dynamic_sidebar('footer_4');
				 ?>
			</div>

			<div class="col-xs-6 col-sm-3 col-md-3 recomm footer_5">
				<?php 
					dynamic_sidebar('footer_5');
				 ?>
			</div>
		</div>
	</div>
	<div style="clear: both;"></div>
	<div class="copy-right">
		<?php 
			dynamic_sidebar('footer_6');
		 ?>
	</div>
</footer>
<div id="back-to-top">
    <a href="javascript:void(0)"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
</div>

<?php 
	if(is_active_sidebar('phone-support')) {
		dynamic_sidebar('phone-support');
	}
 ?>