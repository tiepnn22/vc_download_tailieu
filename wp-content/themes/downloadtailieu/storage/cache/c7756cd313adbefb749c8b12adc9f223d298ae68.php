<!DOCTYPE html>
<html <?php (language_attributes()); ?>>

  <?php echo $__env->make('partials.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <body <?php (body_class()); ?>>
        <div id="fb-root"></div>
    <?php (do_action('get_header')); ?>
    <?php echo $__env->make('partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <div class="wrap container" role="document">
      <div class="content">
        <?php if(!is_home() && !is_front_page()): ?>
        <div class="breadcrumbs">
            <?php 
            if(function_exists('bcn_display')) { 
                echo '<a href="' . site_url() . '">Trang chủ </a><i class="fa fa-caret-right" aria-hidden="true"></i> ';
                bcn_display(); 
            } 
             ?>
        </div>
        <?php endif; ?>

        <main class="main">
            <?php echo $__env->yieldContent('content'); ?>
        </main>
        
      </div>
    </div>

    <?php (do_action('get_footer')); ?>
    <?php echo $__env->make('partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php 
        wp_footer();
        if(is_user_logged_in() && current_user_can('guest')) {
            show_admin_bar(false);
        }
     ?>

    <div class="fb-chatdialog" style="position:relative">
        <button class="btn btn-primary btn-fb-dialog hidden-xs" style="position: fixed; bottom: 0; right: 10px;">Gửi tin nhắn cho chúng tôi</button>
        <button class="btn btn-primary btn-fb-dialog-mobile visible-xs"><i class="fa fa-envelope-open" aria-hidden="true"></i></button>
        <iframe class="fb-iframe" src="https://www.facebook.com/plugins/page.php?href=https:%2F%2Fwww.facebook.com%2FDetoanfilewordvn-1535726859827921&tabs=messages&width=340&height=300&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=484404005277812" width="340" height="300" data-show="none" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
    </div>

    <div class="preload-handle-payment hide">
        <div class="wrapper-load text-center">
            <i class="fa fa-spinner fa-pulse fa-2x fa-fw spinner-loading"></i> Đang xử lý ...
        </div>
    </div>
    </body>

</html>
