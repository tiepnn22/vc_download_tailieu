<article <?php (post_class()); ?>>
    <div class="entry-content">
    	<div class="container">
			<div class="row">
				<div class="col-12 col-sm-9 col-md-9 col-xl-9 pad-l-0-i">
        			<?php (the_content()); ?>
        			<div class="row package-section">
        				<div class="col-4 col-sm-4 col-md-4 col-xl-4 block-1">
        					<div class="col-12 yellow-1-bg">
        						Tiết kiệm
        					</div>
        					<div class="col-12 yellow-2-bg">
        						<span>890.000 <sup>đ</sup></span>
        					</div>
        					<div class="col-12 yellow-3-bg">
        						<p>Nhận</p>
        						<p class="price">3.000.000 <sup>đ</sup></p>
        						<p>vào tài khoản</p>
        					</div>
        					<div class="col-12 white-bg">
        						<p><b>Giảm đển 40%</b> đến hết 31/08/2017 </p>
        						thời gian dử dụng 1 năm có 30 suất được đăng ký trong 1 tháng
        					</div>
        				</div>
        				<div class="col-4 col-sm-4 col-md-4 col-xl-4 block-2">
        					<div class="col-12 green-1-bg">
        						Tiết kiệm
        					</div>
        					<div class="col-12 green-2-bg">
        						<span>890.000 <sup>đ</sup></span>
        					</div>
        					<div class="col-12 green-3-bg">
        						<p>Nhận</p>
        						<p class="price">3.000.000 <sup>đ</sup></p>
        						<p>vào tài khoản</p>
        					</div>
        					<div class="col-12 white-bg">
        						<p><b>Giảm đển 40%</b> đến hết 31/08/2017 </p>
        						thời gian dử dụng 1 năm có 30 suất được đăng ký trong 1 tháng
        					</div>
        				</div>
        				<div class="col-4 col-sm-4 col-md-4 col-xl-4 block-3">
        					<div class="col-12 blue-1-bg">
        						Tiết kiệm
        					</div>
        					<div class="col-12 blue-2-bg">
        						<span>890.000 <sup>đ</sup></span>
        					</div>
        					<div class="col-12 blue-3-bg">
        						<p>Nhận</p>
        						<p class="price">3.000.000 <sup>đ</sup></p>
        						<p>vào tài khoản</p>
        					</div>
        					<div class="col-12 white-bg">
        						<p><b>Giảm đển 40%</b> đến hết 31/08/2017 </p>
        						thời gian dử dụng 1 năm có 30 suất được đăng ký trong 1 tháng
        					</div>
        				</div>
        			</div>
    				<?php echo $__env->make('partials.comments', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				</div>
				<div class="col-12 col-sm-3 col-md-3 col-xl-3 sidebar-section pad-l-0-i pad-r-0-i">
					<?php echo $__env->make('components.sidebar-wpdmpro', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				</div>
			</div>
		</div>
    </div>
    <footer>
        <?php echo wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'vicoders'), 'after' => '</p></nav>']); ?>

    </footer>
</article>
