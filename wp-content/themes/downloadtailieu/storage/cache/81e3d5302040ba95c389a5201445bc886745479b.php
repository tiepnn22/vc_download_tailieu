<div class="list-tailieu">
	<div class="content-list-tailieu">
		<?php if($child_terms): ?>
			<?php $__currentLoopData = $child_terms; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $child): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<div class="title-categori">
					<p class="title"><?php echo e($child->name); ?></p>
				</div>
				<?php 
					$sub_child_terms = getChildTerms($child->term_id);
					if(!empty($sub_child_terms)) {
						echo '<div class="row folder-tailieu">';
							foreach ($sub_child_terms as $sub_val) {
									$count = 0;
									$name_parent_cat = $sub_val->name;
									$url_parent_cat = get_category_link($sub_val->term_id);
									$count += count_nested_categories($sub_val->term_id);
								 ?>
								<div class="col-12 col-md-3 col-sm-6 col-xl-3 list-folder">
									<div class="parent_cat">
										<div class="images_parent_cat">
											<a href="<?php echo e($url_parent_cat); ?>">
												<img class="img-responsive center-block" src="<?php echo e(get_stylesheet_directory_uri()); ?>/resources/assets/images/tai-lieu/folder-tailieu.png" alt="">
											</a>
										</div>

										<div class="name_parent_cat">
											<a href="<?php echo e($url_parent_cat); ?>">
												<p class="title-item fw-600"><?php echo e($name_parent_cat); ?></p>
												<p class="count_post">(<?php echo e($count); ?> tài liệu)</p>
											</a>
										</div>
									</div>
								</div>
								<?php 
							}
						echo '</div>';
					} else {
						$count = 0;
						$name_parent_cat = $child->name;
						$url_parent_cat = get_category_link($child->term_id);
						$count += count_nested_categories($child->term_id);
						echo '<div class="row folder-tailieu">';
							 ?>
							<div class="col-xs-12 col-md-3 col-sm-6 list-folder">
								<div class="parent_cat">
									<div class="images_parent_cat">
										<a href="<?php echo e($url_parent_cat); ?>">
											<img class="img-responsive" src="<?php echo e(get_stylesheet_directory_uri()); ?>/resources/assets/images/tai-lieu/folder-tailieu.png" alt="">
										</a>
									</div>

									<div class="name_parent_cat">
										<a href="<?php echo e($url_parent_cat); ?>">
											<p class="title-item fw-600"><?php echo e($name_parent_cat); ?></p>
											<p class="count_post">(<?php echo e($count); ?> tài liệu)</p>
										</a>
									</div>
								</div>
							</div>
							<?php 
						echo '</div>';
					}
				 ?>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		<?php else: ?>
			<div class="title-categori">
				<p class="title"><?php echo e($current_term->name); ?></p>
			</div>

			<div class="row list-item">
				<?php 
					if(!empty($get_post_categoris->posts)) :
						foreach($get_post_categoris->posts as $key => $val) {
							$link = get_permalink($val->ID);
							 ?>
							<div class="col-xs-12 col-md-3 col-sm-6 recomm-homepage">
								<div class="images-item">
									<a href="<?php echo e($link); ?>">
										<img class="icon-topic" src="<?php echo e(get_stylesheet_directory_uri()); ?>/resources/assets/images/home/icon-work.png" alt="">
										<?php if($val->check_download > 0): ?>
		                                	<p class="btn-violet">Đã tải</p>
		                                <?php endif; ?>
										<img class="avatar-item" style="background-image:url(<?php echo e(wp_get_attachment_url(get_post_thumbnail_id($val->ID))); ?>); " src="<?php echo e(get_stylesheet_directory_uri()); ?>/resources/assets/images/home/item-home.png" alt="">

									</a>
								</div>
								<div class="info-item">
									<a href="<?php echo e($link); ?>">
										<p class="title-item fw-600"><?php echo $val->post_title; ?></p>
									</a>
								</div>
							</div>
							<?php 
						}
					else:
						echo '<div class="col-xs-12 col-md-3 col-sm-6 recomm-homepage fw-600">';
						echo 'Không có dữ liệu';
						echo '</div>';
					endif;
				 ?>
			</div>
		<?php endif; ?>
	</div>
</div>
