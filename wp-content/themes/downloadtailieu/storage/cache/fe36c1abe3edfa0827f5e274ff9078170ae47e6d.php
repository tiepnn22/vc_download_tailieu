	<?php 
		dynamic_sidebar('slider');
	 ?>


<div class="content-homepage">
	<div class="tailieu-homepage">
			<?php 

				foreach ($content as $value){
					// echo "<pre>";
					// var_dump($value);
					
					$url = get_category_link($value['id']);
			 ?>

				<div class="title-categori">
					<p class="title"><?php echo e($value['name']); ?></p>

					<p class="url-list-item"><a href="<?php echo e($url); ?>">Xem tất cả <i class="fa fa-arrow-right" aria-hidden="true"></i></a></p>
				</div>

				<div class="row list-item">
						<?php 
							foreach ($value['posts'] as $post_cat) {

								// var_dump($post_cat);
								
								$title_post = $post_cat->post_title;

								$url = $post_cat->guid;

								$link_url = wp_get_attachment_url(get_post_thumbnail_id($post_cat->ID));
								$highlights = get_field('highlights', $post_cat->ID);

								// var_dump($highlights[0]);

								if($highlights[0] == "co"){
								if(empty($link_url)) {
									$link_url = get_stylesheet_directory_uri().'/resources/assets/images/home/Untitled-1.png';
								}
						 ?>

							<div class="col-xs-12 col-md-3 col-sm-6 recomm-homepage">
								<div class="images-item">
									<a href="<?php echo e($url); ?>">
										<img class="icon-topic" src="<?php echo e(get_stylesheet_directory_uri()); ?>/resources/assets/images/home/icon-work.png" alt="">
											
										<img class="avatar-item" style="background-image:url(<?php echo e($link_url); ?>); " src="<?php echo e(get_stylesheet_directory_uri()); ?>/resources/assets/images/home/item-home.png" alt="">
										<?php if($post_cat->check_download > 0): ?>
		                                <p class="btn-violet">đã tải</p>
		                                <?php endif; ?>
									</a>
								</div>
								<div class="info-item">
									<a href="<?php echo e($url); ?>">
										<p class="title-item"><?php echo e($title_post); ?></p>
									</a>
								</div>
							</div>

						<?php 
								}
							}
						 ?>
				</div>
			<?php 
				}
			 ?>
	</div>
</div>