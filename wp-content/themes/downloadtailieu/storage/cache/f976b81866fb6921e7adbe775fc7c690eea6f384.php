<div class="sidebar-section">
	<div class="title">
		Tài liệu liên quan
	</div>
	<div class="relation-file">
		<?php if(!empty($relate_docs)): ?>
			<?php 
				$i = 0;	
			 ?>
			<?php $__currentLoopData = $relate_docs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php $__currentLoopData = $item->posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item_post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<?php 
						if($i > 17) {
							break;
						}
						$i++;
						$link_doc = get_permalink($item_post->ID);
						$images = wp_get_attachment_url(get_post_thumbnail_id($item_post->ID));
						if(empty($images)) {
							$images = 'http://fakeimg.pl/45x65';
						}

					if ($post_current->ID == $item_post->ID){
						continue;
					}
					 ?>
						<div class="item row">
							<div class="col-6 col-sm-3 col-md-3 col-xl-3 img-area center-block">
								<?php 
									$data = [
										'link_img' => $images,
										'check_download' => $check_download,
										'link_doc' => $link_doc
									];
									echo view('components.item-doc', $data);
								 ?>								
							</div>
							<div class="col-9 col-sm-9 col-md-9 col-xl-9 text-left title-package center-block">
								<div class="title-wrap"><a href="<?php echo get_permalink($item_post->ID); ?>"><?php echo e($item_post->post_title); ?></a></div>
								<div class="view-down-group">
									<span class="view-count">
										<i class="fa fa-eye" aria-hidden="true"></i>&nbsp <?php echo get_package_data($item_post->ID, 'view_count'); ?>

									</span> &nbsp &nbsp
									</span class="donwload-count">
										<?php 
											$download_count = get_package_data($item_post->ID, 'download_count');
											if($download_count == 0){
												$download_count = 0;
											}
										 ?>
										<i class="fa fa-download" aria-hidden="true"></i> &nbsp <?php echo $download_count; ?>

									</span>
								</div>
							</div>
						</div>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		<?php endif; ?>
	</div>
</div>