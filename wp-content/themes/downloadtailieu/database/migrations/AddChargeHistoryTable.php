<?php 

namespace Database\Migration;

use Database\Migration\ExeDB;
use Illuminate\Database\Capsule\Manager as Capsule;

/**
* 
*/
class AddChargeHistoryTable extends ExeDB
{
	public $table = 'charge_history';

	public function __construct()
	{
		parent::__construct();
	}

	public function up()
	{
		global $wpdb;
		$table_name = $wpdb->prefix . $this->table;
		if (!Capsule::Schema()->hasTable($table_name)) {
			Capsule::Schema()->create($table_name, function($table){
				$table->increments('id');
				$table->integer('user_id');
				$table->string('seri_card', 100);
				$table->string('number_card', 100);
				$table->float('amount');
				$table->text('reason')->nullable();
				$table->integer('type')->comment('0:buy - 1:charge - 2:reward');
				$table->string('type_card', 50)->default('viettel')->comment('viettel - vinaphone - mobiphone');
				$table->boolean('status')->default(0);
				$table->timestamps();
			});
		}		
	}

	public function down() {
		global $wpdb;
		$table_name = $wpdb->prefix . $this->table;
		if (Capsule::Schema()->hasTable($table_name)) {
			Capsule::Schema()->drop($table_name);
		}
	}
}