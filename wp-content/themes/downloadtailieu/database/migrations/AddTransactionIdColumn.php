<?php 

namespace Database\Migration;

use Database\Migration\ExeDB;
use Illuminate\Database\Capsule\Manager as Capsule;

/**
* 
*/
class AddTransactionIdColumn extends ExeDB
{
	public $table = 'charge_history';

	public function __construct()
	{
		parent::__construct();
	}

	public function up()
	{
		global $wpdb;
		$table_name = $wpdb->prefix . $this->table;
		if (Capsule::Schema()->hasTable($table_name)) {
			Capsule::Schema()->table($table_name, function($table){
				$table->string('transaction_id')->after('user_id')->default('000000');
			});
		}		
	}

	public function down() {
		global $wpdb;
		$table_name = $wpdb->prefix . $this->table;
		if (Capsule::Schema()->hasTable($table_name)) {
			Capsule::Schema()->dropColumn('transaction_id');
		}
	}
}