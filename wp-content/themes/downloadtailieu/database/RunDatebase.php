<?php

namespace Database;

use Database\Migration\AddChargeHistoryTable;
use Database\Migration\AddTransactionHistoryTable;
use Database\Migration\AddTransactionIdColumn;
use Database\Migration\AddUserExtendTable;
use Database\Seeder\UserExtendSeeder;

/**
 * Class run migrate database
 */
class RunDatebase
{
    protected $list_tables = [
        AddChargeHistoryTable::class,
        AddTransactionHistoryTable::class,
        AddUserExtendTable::class,
        AddTransactionIdColumn::class
    ];

    public function __construct() {}

    /**
     * [up create table on ddatabase]
     * @return void [description]
     */
    public function up()
    {
        if (current_user_can('administrator')):
            foreach ($this->list_tables as $key => $value) {
                $class = new $value();
                $class->up();
            }
        endif;
    }

    /**
     * [down destroy table on database]
     * @return void [description]
     */
    public function down()
    {
        if (current_user_can('administrator')):
            foreach ($this->list_tables as $key => $value) {
                $class = new $value();
                $class->down();
            }
        endif;
    }

    public function seeder()
    {
        $seeders = [
            UserExtendSeeder::class,
        ];
        foreach ($seeders as $key => $value) {
            $class = new $value();
        }
    }
}
