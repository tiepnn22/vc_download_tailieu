<?php
global $current_user, $wpdb;

$params = [
    'hierarchical'     => 1,
    'show_option_none' => '',
    'hide_empty'       => 0,
    'parent'           => 0,
    'taxonomy'         => 'wpdmcategory',
];

$categories = get_categories($params);

foreach ($categories as $val_cates2) {

    $args = [
        'post_type'      => 'wpdmpro',
        'posts_per_page' => 10,
        'tax_query'      => array(
            array(
                'taxonomy'         => 'wpdmcategory',
                'terms'            => array($val_cates2->term_id),
                'field'            => 'term_id',
                'operator'         => 'AND',
                'include_children' => false,
            ),
        ),
    ];

    $loop = new WP_Query($args);
    foreach ($loop->posts as $key => $val) {
        $get_count_download = $wpdb->get_row("select count(id) as count_dl from {$wpdb->prefix}ahm_download_stats s where s.uid = '{$current_user->ID}' and s.pid = '{$val->ID}'");
        $val->check_download = $get_count_download->count_dl;
    }

    $content[] = [
        'id'    => $val_cates2->term_id,
        'name'  => $val_cates2->name,
        'posts' => $loop->posts
    ];
}

$data = [
    'content' => $content,
    'check_download' => $get_count_download->count_dl,
];

view('index', $data);
