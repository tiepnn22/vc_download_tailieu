<?php

namespace App\Models;

use NF\Database\Eloquent\Model;

/**
 *
 */
class Transaction extends Model
{
    /**
     * [$table_name name of table]
     * @var string
     */
    protected $table = PREFIX_TABLE . 'transaction_history';

    /**
     * [$primary_id primary key of table]
     * @var string
     */
    protected $primary_key = 'id';

    const STATUS_TRUE  = 1;
    const STATUS_FALSE = 0;

    protected $fillable = ['user_id', 'doc_id', 'trans_id', 'status', 'created_at', 'updated_at'];

}
