<?php 

namespace App\RestApi;

use App\Models\Charge;
use App\Models\UserExtend;

/**
* 
*/
class RegisterSuccess extends \WP_REST_Controller
{
	/**
     * [$base The base to use in the API route]
     * @var string
     */
    protected $rest_base = 'register-success';

    /**
     * [$namespace namespace for routes API]
     * @var string
     */
    protected $namespace = 'wp/v2';
	public function __construct()
	{
		add_action('rest_api_init', [$this, 'register_routes']);
	}

	public function register_routes() {
		register_rest_route($this->namespace, "/{$this->rest_base}", [
            'methods'             => \WP_REST_Server::EDITABLE,
            'callback'            => [$this, 'handle_register_success'],
            'permission_callback' => [$this, 'update_items_permissions_check'],
        ]);
	}

	public function handle_register_success($request) {
		global $wpdb;
		$params = $request->get_params();
		if(empty($params) || empty($params['user_id'])) {
			return json_encode(['code' => 0, 'message' => 'Không xác định được người dùng']);
		}
		$charge = new Charge();
		$charge->user_id = $params['user_id'];
		$charge->seri_card = 0;
		$charge->number_card = 0;
		$charge->amount = 10000;
		$charge->reason = 'Đăng ký tài khoản mới';
		$charge->type = 2;
		$charge->status = 1;
		$result = $charge->save();
		if($result) {
			$user_extend = UserExtend::where('user_id', $params['user_id'])->first();
			if($user_extend) {
				$user_extend->save_money = 10000;
				$user_extend->save();
			} else {
				$new_extend = new UserExtend();
				$new_extend->user_id = $params['user_id'];
				$new_extend->save_money = 10000;
				$new_extend->save();
			}
			return json_encode(['code' => 200, 'message' => 'Cập nhật thành công']);
		} else {
			return json_encode(['code' => 404, 'message' => 'Không lưu được thông tin']);
		}
	}

	public function update_items_permissions_check() {
		return true;
	}
}