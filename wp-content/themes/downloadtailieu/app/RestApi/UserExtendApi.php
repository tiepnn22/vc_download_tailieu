<?php 

namespace App\RestApi;

use App\Models\UserExtend;

/**
* 
*/
class UserExtendApi extends \WP_REST_Controller
{
	/**
     * [$base The base to use in the API route]
     * @var string
     */
    protected $rest_base = 'user-extend-request';

    /**
     * [$namespace namespace for routes API]
     * @var string
     */
    protected $namespace = 'wp/v2';
	public function __construct()
	{
		add_action('rest_api_init', [$this, 'register_routes']);
	}

	public function register_routes() {
		register_rest_route($this->namespace, "/{$this->rest_base}", [
            'methods'             => \WP_REST_Server::EDITABLE,
            'callback'            => [$this, 'update_item'],
            'permission_callback' => [$this, 'update_items_permissions_check'],
        ]);
	}

	public function update_item($request) {
		global $wpdb;
		$params = $request->get_params();
		if(empty($params)) {
			return json_encode(['code' => 406, 'message' => '<div class"alert alert-danger">No content</div>']);
		}
		$user_id = (!empty($params['user_id']) ? $params['user_id'] : 0);
		$user_ex = UserExtend::where('user_id', $user_id)->first();

		if(!empty($user_ex)) {
			$wpdb->update(
				'wp_users', 
				[
					'user_email' => $params['inp_email'], 
					'display_name' => $params['inp_displayname']
				],
				[
					'ID' => $user_id,
				]
			);
			$user_ex->dob = $params['inp_dob'];
			$user_ex->job = $params['inp_job'];
			$user_ex->subject = $params['inp_subject'];
			$user_ex->school = $params['inp_school'];
			$user_ex->phone = $params['inp_phone'];
			$user_ex->address = $params['inp_address'];
			$user_ex->payment_info = $params['inp_infoaccount'];
			$user_ex->save();
			return json_encode(['code' => 200, 'message' => '<div class="alert alert-success">Thông tin đã được cập nhật thành công</div>']);
		} else {
			return json_encode(['code' => 406, 'message' => '<div class="alert alert-danger">Không xác định người dùng hiện tại</div>']);
		}
	}

	public function update_items_permissions_check() {
		return true;
	}
}