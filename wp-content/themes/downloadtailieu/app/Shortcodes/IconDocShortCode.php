<?php

namespace App\Shortcodes;

use NF\Abstracts\ShortCode;

class IconDocShortCode extends ShortCode
{
    public $name = 'icon_doc';

    public function render($attrs)
    {
    	$icon_doc_img = '<img class="icon-topic" src="' . get_stylesheet_directory_uri() . '/resources/assets/images/home/icon-work.png" alt="">';
    	return $icon_doc_img;
    }
}
