<?php
namespace App\Widgets;

use MSC\Widget;

class TableListCardWidget extends Widget
{
    public function __construct()
    {
        $widget = [
            'id'          => __('tablelistcard-widget', 'textdomain'),
            'label'       => __('Quy đổi thẻ nạp', 'textdomain'),
            'description' => __('List charge card on baokim', 'textdomain'),
        ];

        $fields = [
            [
                'label' => __('Mệnh giá 10.000', 'downloadtailieu'),
                'name'  => 'card10',
                'type'  => 'text',
            ],
            [
                'label' => __('Mệnh giá 20.000', 'downloadtailieu'),
                'name'  => 'card20',
                'type'  => 'text',
            ],
            [
                'label' => __('Mệnh giá 50.000', 'downloadtailieu'),
                'name'  => 'card50',
                'type'  => 'text',
            ],
            [
                'label' => __('Mệnh giá 100.000', 'downloadtailieu'),
                'name'  => 'card100',
                'type'  => 'text',
            ],
            [
                'label' => __('Mệnh giá 200.000', 'downloadtailieu'),
                'name'  => 'card200',
                'type'  => 'text',
            ],
            [
                'label' => __('Mệnh giá 500.000', 'downloadtailieu'),
                'name'  => 'card500',
                'type'  => 'text',
            ]
        ];

        parent::__construct($widget, $fields);
    }

    public function handle($instance)
    {
        ?>
        <div class="col-12 value-payment">
            <table class="table table-bordered">
                <tr>
                    <th>Mệnh giá thẻ cào</th>
                    <th>Tiền nhận được</th>
                </tr>
                <tr>
                    <td>Nạp thẻ mệnh giá 10.000 vnđ</td> 
                    <td>Nhận <?php echo $instance['card10']; ?> đ vào tài khoản</td> 
                </tr>
                <tr>
                    <td>Nạp thẻ mệnh giá 20.000 vnđ</td> 
                    <td>Nhận <?php echo $instance['card20']; ?> đ vào tài khoản</td> 
                </tr>
                <tr>
                    <td>Nạp thẻ mệnh giá 50.000 vnđ</td> 
                    <td>Nhận <?php echo $instance['card50']; ?> đ vào tài khoản</td> 
                </tr>
                <tr>
                    <td>Nạp thẻ mệnh giá 100.000 vnđ</td> 
                    <td>Nhận <?php echo $instance['card100']; ?> đ vào tài khoản</td> 
                </tr>
                <tr>
                    <td>Nạp thẻ mệnh giá 200.000 vnđ</td> 
                    <td>Nhận <?php echo $instance['card200']; ?> đ vào tài khoản</td> 
                </tr>
                <tr>
                    <td>Nạp thẻ mệnh giá 500.000 vnđ</td> 
                    <td>Nhận <?php echo $instance['card500']; ?> đ vào tài khoản</td> 
                </tr>
            </table>
        </div>
        <?php
	}
}
