<?php
namespace App\Widgets;

use MSC\Widget;

class BlockPackagePromoWidget extends Widget
{
    public function __construct()
    {
        $widget = [
            'id'          => __('blockpackagepromo-widget', 'downloadtailieu'),
            'label'       => __('Khối các gói tiết kiệm', 'downloadtailieu'),
            'description' => __('Hiển thị các gói tiết kiệm trên trang chi tiết tài liệu', 'downloadtailieu'),
        ];

        $fields = [
            [
                'label' => __('Mô tả', 'downloadtailieu'),
                'name'  => 'price_package',
                'type'  => 'textarea'
            ],
            [
                'label' => __('Nhận được', 'downloadtailieu'),
                'name'  => 'receive_price',
                'type'  => 'text'
            ],
            [
                'label' => __('Giá gói', 'downloadtailieu'),
                'name'  => 'charge_price',
                'type'  => 'text'
            ],
            [
                'label' => __('Chọn layout', 'downloadtailieu'),
                'name'  => 'choice_block',
                'type'  => 'select',
                'options' => [
                    'yellow' => 'Block Yellow',
                    'green' => 'Block Green',
                    'blue' => 'Block Blue'
                ]
            ],
        ];

        parent::__construct($widget, $fields);
    }

    public function handle($instance)
    {
        $this->BlockHtml($instance['choice_block'], $instance['msc_title'], $instance['charge_price'], $instance['receive_price'], $instance['price_package']);
	}

    public function BlockHtml($color_block, $title, $charge_price, $receive_price, $desc = '') {
        $number_block = '';
        switch ($color_block) {
            case 'yellow':
                $number_block = 1;
                break;
            case 'green':
                $number_block = 2;
                break;
            case 'blue':
                $number_block = 3;
                break;
            default:
                $number_block = 1;
                break;
        }
        ?>
        <div class="block-<?php echo $number_block; ?>">
            <div class="col-12 <?php echo $color_block; ?>-1-bg">
                <?php echo $title; ?>
            </div>
            <div class="col-12 <?php echo $color_block; ?>-2-bg">
                <span><?php echo $charge_price; ?> <sup>đ</sup></span>
            </div>
            <div class="col-12 <?php echo $color_block; ?>-3-bg">
                <p>Nhận</p>
                <p class="price"><?php echo $receive_price; ?> <sup>đ</sup></p>
                <p>vào tài khoản</p>
            </div>
            <div class="col-12 white-bg">
                <?php echo $desc; ?>
            </div>
        </div>
        <?php
    }
}
