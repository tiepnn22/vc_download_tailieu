<?php
namespace App\Widgets;

use MSC\Widget;

class PhoneSupportWidget extends Widget
{
    public function __construct()
    {
        $widget = [
            'id'          => __('phone-support-widget', 'downloadtailieu'),
            'label'       => __('Phone-Support', 'downloadtailieu'),
            'description' => __('Phone support', 'downloadtailieu'),
        ];

        $fields = [
            [
                'label' => __('Text phone', 'downloadtailieu'),
                'name'  => 'text-phone',
                'type'  => 'text',
            ],
            [
                'label' => __('Phone', 'downloadtailieu'),
                'name'  => 'phone',
                'type'  => 'text',
            ],
        ];

        parent::__construct($widget, $fields);
    }

    public function handle($instance)
    {
        ?>
        <div class="hidden-xs phone-support opensan">
            <div class="phone-wrap">
                <a href="tel:<?php echo $instance['phone']; ?>"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $instance['text-phone'] ?></a>
            </div>
        </div>
        <div class="phone-support-mobile visible-xs">
            <div class="phone-wrap text-center">
                <a href="tel:<?php echo $instance['phone']; ?>"><i class="fa fa-phone" aria-hidden="true"></i></a>
            </div>
        </div>
        <?php
	}
}
