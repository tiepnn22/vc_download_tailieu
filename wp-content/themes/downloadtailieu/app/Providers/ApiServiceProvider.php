<?php

namespace App\Providers;

use App\RestApi\BaokimPayment;
use App\RestApi\RegisterSuccess;
use App\RestApi\TestMenu;
use App\RestApi\UserExtendApi;
use Illuminate\Support\ServiceProvider;

class ApiServiceProvider extends ServiceProvider
{
    public $listen = [
        BaokimPayment::class,
        UserExtendApi::class,
        RegisterSuccess::class,
        TestMenu::class
    ];

    public function register()
    {
        foreach ($this->listen as $class) {
            $this->resolveApi($class);
        }
    }

    /**
     * Resolve a restapi instance from the class name.
     *
     * @param  string  $restapi
     * @return restapi instance
     */
    public function resolveApi($restapi)
    {
        return new $restapi();
    }
}
