<?php
/**
 * Template Name: Template Payment Baokim
 *
 */
global $post;
global $current_user, $wpdb;
$res = $wpdb->get_row("select p.save_money, p.has_money from {$wpdb->prefix}user_extend p where p.user_id = '{$current_user->ID}'");

$data = [
    'post'        => $post,
    'user_extend' => $res,
];

if(is_user_logged_in()){
	view('templates.payment', $data);
} else {
	wp_redirect('login');
}
	

