<?php
if (!defined('ABSPATH')) {
    die('Error!');
}

use App\Models\Transaction;
use App\Models\UserExtend;
global $current_user, $wpdb;
$post_id = $post->ID;

$category = get_the_terms($post_id, 'wpdmcategory');
if (!empty($category)) {
    foreach ($category as $cat) {
        $cat_parent = $cat->parent;

        if ($cat_parent == 0) {
            $cat_parent = $cat->term_id;
        }

        $args_cat = [
            'post_type'      => 'wpdmpro',
            'posts_per_page' => 8,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'tax_query'      => [
                [
                    'taxonomy' => 'wpdmcategory',
                    'field'    => 'id',
                    'terms'    => $cat_parent,
                ],
            ],
        ];
        $get_cat[] = new WP_Query($args_cat);
    }
    //=======================
    // echo "<pre>";
    // var_dump(count($get_cat));
    // die;
    $user_ex   = UserExtend::where('user_id', $current_user->ID)->first();
    $has_money = $user_ex->has_money;

    $transaction  = Transaction::where('user_id', $current_user->ID)->where('doc_id', $post_id)->where('status', 1)->first();
    $can_download = 1;
    if (!$transaction) {
        $can_download = 0;
    }

    $permalink       = get_permalink($post_id);
    $url_preview     = getLinkForPreview($post_id);
    $base_price      = get_package_data($post_id, 'base_price');
    $files           = get_package_data($post_id, 'files');
    $check_type_file = 'pdf';
    foreach ($files as $i => $sfile) {
        $ifile           = $sfile;
        $sfile           = explode(".", $sfile);
        $check_type_file = end($sfile);
    }
    // var_dump($check_type_file); die;
    $curr_post = get_post($post_id);
    $link_img  = wp_get_attachment_url(get_post_thumbnail_id($post_id));
    if (empty($link_img)) {
        $link_img = 'http://fakeimg.pl/118x168';
    }
    $get_count_download = $wpdb->get_row("select count(id) as count_dl from {$wpdb->prefix}ahm_download_stats s where s.uid = '{$current_user->ID}' and s.pid = '{$post_id}'");

    $count_view = get_package_data($post_id, 'view_count');
    if (empty($count_view) || ($count_view == '')) {
        $count_view = 0;
    }

    $count_download = get_package_data($post_id, 'download_count');
    if (empty($count_download) || ($count_download == '')) {
        $count_download = 0;
    }

    $data = [
        'relate_docs'     => $get_cat,
        'post_id'         => $post_id,
        'base_price'      => $base_price,
        'url_preview'     => $url_preview,
        'permalink'       => $permalink,
        'title'           => $curr_post->post_title,
        'description'     => $curr_post->post_content,
        'excerpt'     => $curr_post->post_excerpt,
        'link_img'        => $link_img,
        'check_download'  => $get_count_download->count_dl,
        'has_money'       => $has_money,
        'can_download'    => $can_download,
        'count_view'      => $count_view,
        'count_download'  => $count_download,
        'files'           => $files,
        'check_type_file' => $check_type_file,
    ];
}

view('single', $data);
