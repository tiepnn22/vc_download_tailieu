<?php
global $wpdb, $current_user;

$items_per_page = 30;
$paged = isset($_GET['trang']) ? ($_GET['trang']) : 0;
$start = isset($_GET['trang'])?($_GET['trang']-1)*$items_per_page:0;

$res = $wpdb->get_results("select c.* from {$wpdb->prefix}charge_history c where c.user_id = '{$current_user->ID}' limit $start, $items_per_page");

// $transaction = $wpdb->get_results("select p.post_title, s.* from {$wpdb->prefix}posts p, {$wpdb->prefix}transaction_history s where s.user_id = '{$current_user->ID}' and s.doc_id = p.ID limit $start, $items_per_page");

// foreach ($transaction as $key => $item_tran) {
// 	$item_tran->price = get_package_data($item_tran->doc_id, 'base_price');
// }

?>
<div class="transaction-section">
	<span class="h3-title">Lịch sử giao dịch của </span><span class="user_login"><?php echo $current_user->user_login; ?></span>
	<table class="table table-bordered">
		<thead class="text-center thead">
			<tr class="text-center">
				<th scope="col">Mô tả</th>
				<th scope="col">Loại thẻ</th>
				<th scope="col">Mã Pin</th>
				<th scope="col">Sericard</th>
				<th scope="col">Trạng thái</th>
				<th scope="col">Số tiền</th>
				<th scope="col">Thời gian</th>
			</tr>
		</thead>
		<tbody class="text-center">
			<?php 
			if(!empty($res)){
				foreach ($res as $key => $val) {
					?>
					<tr>
						<td data-label="Mô tả"><?php echo $val->reason ?></td>
						<td data-label="Loại thẻ"><?php echo (!empty($val->type_card) ? $val->type_card : '<trống>'); ?></td>
						<td data-label="Mã Pin"><?php echo (!empty($val->number_card) ? $val->number_card : '<trống>'); ?></td>
						<td data-label="Seri card"><?php echo (!empty($val->seri_card) ? $val->seri_card : '<trống>'); ?></td>
						<td data-label="Trạng thái">
							<?php  
							if($val->type == 0) {
								echo 'Mua';
							} elseif($val->type == 1) {
								echo 'Nạp';
							} elseif($val->type == 2) {
								echo 'Thưởng';
							} else {
								echo 'Không xác định';
							}
							?>
						</td>
						<td data-label="Số tiền"><?php echo price_format($val->amount); ?> vnđ</td>
						<td data-label="Thời gian"><?php echo $val->created_at; ?></td>
					</tr>
					<?php
				}
			} else {
				echo '<tr><td colspan="7">Không có dữ liệu</td></tr>';
			}
			?>
		</tbody>
	</table>
	<div class="paginate-section">
		<?php
			$total_pages = (($wpdb->get_var("select count(*) from {$wpdb->prefix}charge_history where user_id = '{$current_user->ID}'")))/$items_per_page;

            do_action('custom_paginate', $paged, $total_pages);
        ?>
	</div>
</div>