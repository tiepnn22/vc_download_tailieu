<?php 
    global $wp_rewrite, $wp_query;
    $post_type = 'wpdmpro';
    
    $items_per_page = 30;
    $paged = isset($_GET['trang']) ? ($_GET['trang']) : 0;
    $start = isset($_GET['trang'])?($_GET['trang']-1)*$items_per_page:0;
    
    $res = $wpdb->get_results("select * from {$wpdb->prefix}posts p where p.post_author = '{$current_user->ID}' and p.post_type = '{$post_type}' and p.post_status = 'publish' limit $start, $items_per_page");
?>
<div class="wrapper-uploadhistory">
    <div class="">
        <div class="h3-title">Thông tin tài liệu đã tải lên</div>
        <table class="table table-garung table-center table-bordered">
            <thead class="text-center thead">
                <tr class="text-center">
                    <th>STT</th>
                    <th><?php _e('Tài liệu','wpdmpro'); ?></th>
                    <th><?php _e('Ngày đăng','wpdmpro'); ?></th>
                </tr>
            </thead>
            <tbody>
            <?php
            if(!empty($res)) {
                foreach($res as $stt => $stat){
                    ?>
                    <tr>
                        <td><?php echo ($stt + 1); ?></td>
                        <td><a href="<?php echo get_permalink($stat->ID); ?>"><?php echo $stat->post_title; ?></a></td>
                        <td><?php echo $stat->post_date; ?></td>
                    </tr>
                    <?php
                }
            } else {
                echo '<tr><td colspan="3">Không có dữ liệu</td></tr>';
            }
            ?>

            </tbody>
        </table>
        <div class="">
            <?php
                $total_pages = (($wpdb->get_var("select count(*) from {$wpdb->prefix}posts p where p.post_author = '{$current_user->ID}' and p.post_type = '{$post_type}' and p.post_status = 'publish'")))/$items_per_page;
                do_action('custom_paginate', $paged, $total_pages);
            ?>
        </div>
    </div>
</div>