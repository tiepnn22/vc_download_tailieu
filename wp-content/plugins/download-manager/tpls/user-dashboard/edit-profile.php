<?php
global $current_user, $wpdb;
$user = get_userdata($current_user->ID);

$res = $wpdb->get_row("select p.* from {$wpdb->prefix}user_extend p where p.user_id = '{$current_user->ID}'");

?>
<div class="wrapper-editprofile">
    <div id="edit-profile-form">

        <?php if(isset($_SESSION['member_error'])){ ?>
            <div class="alert alert-error"><b>Save Failed!</b><br/><?php echo implode('<br/>',$_SESSION['member_error']); unset($_SESSION['member_error']); ?></div>
        <?php } ?>
        <?php if(isset($_SESSION['member_success'])){ ?>
            <div class="alert alert-success"><b>Done!</b><br/><?php echo $_SESSION['member_success']; unset($_SESSION['member_success']); ?></div>
        <?php } ?>
        <div class="result-save"></div>
    <div class="row">
        <div class="col-sm-9">
            <form method="post" id="edit_profile" name="contact_form" action="" class="form">
                <table class="table table-noborder">
                    <tr>
                        <td>
                            <label><?php echo __('Email', 'wpdbpro'); ?>:(<span class="text-red ">*</span>)</label>
                        </td>
                        <td>
                            <input type="email" class="required form-control" name="inp_email" value="<?php echo $user->user_email;?>" id="inp_email" required="required">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label><?php echo __('Họ Tên', 'wpdbpro'); ?>:(<span class="text-red ">*</span>)</label>
                        </td>
                        <td>
                            <input type="text" class="required form-control" value="<?php echo $user->display_name;?>" name="inp_displayname" id="inp_displayname" required="required">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label><?php echo __('Năm sinh', 'wpdbpro'); ?>:(<span class="text-red ">*</span>)</label>
                        </td>
                        <td>
                            <input type="date" class="required form-control" value="<?php echo $res->dob; ?>" id="inp_dob" name="inp_dob" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" format="YY-mm-dd" required="required">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label><?php echo __('Bạn là', 'wpdbpro'); ?>:</label>
                        </td>
                        <td>
                            <select class="inp_job fix-height-30 form-control" name="inp_job" id="inp_job" style="width: auto;">
                                <option value="hocsinh" <?php echo ($res->job == 'hocsinh' ? 'selected' : ''); ?>>Học sinh</option>
                                <option value="giaovien" <?php echo ($res->job == 'giaovien' ? 'selected' : ''); ?>>Giáo viên</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label><?php echo __('Bộ môn', 'wpdbpro'); ?>:</label>
                        </td>
                        <td>
                            <input type="text" class="required form-control" value="<?php echo $res->subject; ?>" name="inp_subject" id="inp_subject">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label><?php echo __('Trường học', 'wpdbpro'); ?>:</label>
                        </td>
                        <td>
                            <input type="text" class="required form-control" value="<?php echo $res->school; ?>" name="inp_school" id="inp_school">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label><?php echo __('Nơi ở', 'wpdbpro'); ?>:</label>
                        </td>
                        <td>
                            <input type="text" class="required form-control" value="<?php echo $res->address; ?>" name="inp_address" id="inp_address">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label><?php echo __('Phone', 'wpdbpro'); ?>:</label>
                        </td>
                        <td>
                            <input type="text" class="required form-control" value="<?php echo $res->phone; ?>" name="inp_phone" id="inp_phone">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label><?php echo __('Thông tin tài khoản', 'wpdbpro'); ?>:</label>
                        </td>
                        <td>
                            <textarea id="inp_infoaccount" class="form-control" name="inp_infoaccount" rows="5" style="width: 100%;"><?php echo $res->payment_info; ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <i class="fa fa-spinner fa-pulse fa-2x fa-fw spinner-loading hide text-right pull-right"></i>
                            <button type="button" class="btn btn-md btn-success pull-right" id="submit_edit_profile"><?php echo __('Cập nhật thông tin', 'wpdbpro'); ?></button>
                        </td>
                    </tr>
                </table>
                <?php //do_action("wpdm_edit_profile_form"); ?>
            </form>
        </div>
    </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($){
        $('#submit_edit_profile').on('click', function(){
            var inp_email = $("#inp_email").val();
            var inp_displayname = $("#inp_displayname").val();
            var inp_dob = $("#inp_dob").val();
            var inp_job = $("#inp_job").val();
            var inp_subject = $("#inp_subject").val();
            var inp_phone = $("#inp_phone").val();
            var inp_school = $("#inp_school").val();
            var inp_address = $("#inp_address").val();
            var inp_infoaccount = $("#inp_infoaccount").val();

            var required_text = '';
            var flag_error = false;
            if(inp_email == '' || inp_email == null) {
                required_text = '<div class="alert alert-danger"><b>Email không được để trống</b></div>';
                flag_error = true;
            }

            if(inp_displayname == '' || inp_displayname == null) {
                required_text = '<div class="alert alert-danger"><b>Họ tên không được để trống</b></div>';
                flag_error = true;
            }

            if(inp_dob == '' || inp_dob == null) {
                required_text = '<div class="alert alert-danger"><b>Ngày sinh không được để trống</b></div>';
                flag_error = true;
            }

            if(flag_error) {
                $('.result-save').html(required_text);
                $('html, body').animate({scrollTop: 0}, "slow");
                return;
            }

            $.ajax({
                url: base_url + '/wp-json/wp/v2/user-extend-request',
                type: "POST",
                data: {
                    inp_email: inp_email,
                    inp_displayname: inp_displayname,
                    inp_dob: inp_dob,
                    inp_job: inp_job,
                    inp_subject: inp_subject,
                    inp_phone: inp_phone,
                    inp_school: inp_school,
                    inp_address: inp_address,
                    inp_infoaccount: inp_infoaccount,
                    user_id: <?php echo $current_user->ID; ?>
                },
                beforeSend: function() {
                    $('.spinner-loading.fa-spinner').removeClass('hide');
                    $('#submit_edit_profile').addClass('hide');
                }, 
                complete: function(){
                    $('#submit_edit_profile').removeClass('hide');
                    $('.spinner-loading.fa-spinner').addClass('hide');
                },
                success: function(response) {
                    var _respon = $.parseJSON(response);
                    $('.result-save').html(_respon.message);
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                }
            });
        });
    });
</script>
