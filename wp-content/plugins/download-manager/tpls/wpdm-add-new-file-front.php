<?php

if(!defined('ABSPATH')) die();

if(isset($pid))
$post = get_post($pid);
else {
$post = new stdClass();
$post->ID = 0;
$post->post_title = '';
$post->post_content = '';
}

if(isset($hide)) $hide = explode(',',$hide);
else $hide = array();
?>
<div class="w3eden">
 <link rel="stylesheet" type="text/css" href="<?php echo plugins_url('/download-manager/assets/css/chosen.css'); ?>" />
<style>
    .cat-panel ul,
    .cat-panel label,
    .cat-panel li{
        padding: 0;
        margin: 0;
        font-size: 9pt;
    }
    .cat-panel ul{
        margin-left: 20px;
    }
    .cat-panel > ul{
        padding-top: 10px;
    }
</style>
<?php 
    $terms = get_terms( array(
        'taxonomy' => 'wpdmcategory',
        'hide_empty' => false,
    ) );
?>
<div class="wpdm-front">
    <form id="wpdm-pf" action="" method="post">
        <div class="row">
            <div class="col-md-12">
                <input type="hidden" id="act" name="act" value="<?php echo $task =='edit-package'?'_ep_wpdm':'_ap_wpdm'; ?>" />
                <input type="hidden" name="id" id="id" value="<?php echo isset($pid)?$pid:0; ?>" />
                <table class="table table-custom-upload">
                    <tr>
                        <td class="hidden-xs"><label>Tiêu đề tài liệu:</label></td>
                        <td>
                            <label class="visible-xs">Tiêu đề tài liệu:</label>
                            <input id="title" class="form-control"  placeholder="Nhập tiêu đề tại đây" type="text" value="<?php echo isset($post->post_title)?$post->post_title:''; ?>" name="pack[post_title]" />
                        </td>
                    </tr>
                    <tr>
                        <td class="hidden-xs"><label>Chọn file:</label></td>
                        <td>
                            <label class="visible-xs">Chọn file:</label>
                            <div class="panel panel-default" id="package-settings-section">
                                <div class="panel-body">
                                    <?php
                                    require_once dirname(__FILE__) . "/metaboxes/attach-file-front-vicoders.php";
                                    ?>
                                </div>
                            </div>
                            <div class="panel panel-default" id="attached-files-section">
                                <div class="panel-body-ff">
                                    <?php
                                    require_once dirname(__FILE__) . "/metaboxes/attached-files-front.php";
                                    ?>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="hidden-xs"><label>Lớp:</label></td>
                        <td>
                            <label class="visible-xs">Lớp:</label>
                            <select name="docs-grade" id="docs-grade" class="form-control">
                                <option value="10" selected="selected">Lớp 10</option>
                                <option value="11">Lớp 11</option>
                                <option value="12">Lớp 12</option>
                                <option value="0">Khác</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="hidden-xs"><label>Nguồn:</label></td>
                        <td>
                            <label class="visible-xs">Nguồn:</label>
                            <div class="input-group">
                                <input type="radio" name="source-upload" value="1" checked="checked"> Sưu tầm
                                &nbsp &nbsp<input type="radio" name="source-upload" value="2"> Tự làm
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="hidden-xs"><label>Môn:</label></td>
                        <td>
                            <label class="visible-xs">Môn:</label>
                            <select name="cats" id="docs-subject" class="form-control">
                                <?php 
                                foreach ($terms as $key => $term) {
                                    echo '<option value="'. $term->term_id . '">' . ($term->name) . '</option>';
                                }
                                ?>  
                                <option value="0">Khác</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="hidden-xs"><label>Dạng tài liệu:</label></td>
                        <td>
                            <label class="visible-xs">Dạng tài liệu:</label>
                            <div class="input-group">
                                <input type="radio" name="typedoc-upload" value="1" checked="checked"> Đề thi, bài tập theo dạng <br>
                                <input type="radio" name="typedoc-upload" value="2"> Tài liệu, bài giảng , chuyên đề, sách tham khảo
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="hidden-xs"><label>Mô tả chi tiết:</label></td>
                        <td>
                            <label class="visible-xs">Mô tả chi tiết:</label>
                            <div  class="form-group">
                                <textarea id="post_content" name="pack[post_excerpt]" rows="5" style="width: 100%;border: 1px solid #ccc;"><?php (isset($post)? $post->post_content : '' ); ?></textarea>
                                </br>
                                <div class="wrap-btn-submit pull-right">
                                    <button type="submit" accesskey="p" tabindex="5" id="publish" class="btn btn-success text-left opensan" name="publish"><?php echo $task=='EditPackage'?__('Cập nhật','wpdmpro'):__('Đăng tài liệu','wpdmpro'); ?></button>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript" src="<?php echo plugins_url('/download-manager/assets/js/chosen.jquery.min.js'); ?>"></script>
      <script type="text/javascript">
      
      jQuery(document).ready(function() {

        jQuery('select').chosen();
        jQuery('span.infoicon').css({color:'transparent',width:'16px',height:'16px',cursor:'pointer'}).tooltip({placement:'right',html:true});
        jQuery('span.infoicon').tooltip({placement:'right'});
        jQuery('.nopro').click(function(){
            if(this.checked) jQuery('.wpdmlock').removeAttr('checked');
        });
        
        jQuery('.wpdmlock').click(function(){      
         
            if(this.checked) {   
            jQuery('#'+jQuery(this).attr('rel')).slideDown();
            jQuery('.nopro').removeAttr('checked');
            } else {
            jQuery('#'+jQuery(this).attr('rel')).slideUp();    
            }
        });
          
       // jQuery( "#pdate" ).datepicker({dateFormat:'yy-mm-dd'});
       // jQuery( "#udate" ).datepicker({dateFormat:'yy-mm-dd'});
        
        jQuery('#wpdm-pf').submit(function(){
             var editor = tinymce.get('post_content');
             editor.save();
             jQuery('#psp').removeClass('fa-save').addClass('fa-spinner fa-spin');
             jQuery('#publish').attr('disabled','disabled');
             jQuery('#wpdm-pf').ajaxSubmit({
                 //dataType: 'json',
                 beforeSubmit: function() { jQuery('#sving').fadeIn(); },
                 success: function(res) {  
                  jQuery('#sving').fadeOut(); jQuery('#nxt').slideDown();
                  console.log(res);
                     if(res.result=='_ap_wpdm') {
                         location.href = "<?php the_permalink(); ?>/edit-package/"+res.id+"/";
                         jQuery('#wpdm-pf').prepend('<div class="alert alert-success">Package Created Successfully. Opening Edit Window ... <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a></div>');
                     }
                     else {
                         jQuery('#psp').removeClass('fa-spinner fa-spin').addClass('fa-save');
                         jQuery('#publish').removeAttr('disabled');
                         jQuery('#wpdm-pf').prepend('<div class="alert alert-success">Package Updated Successfully <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a></div>');
                     }
                 }
             });
             return false;
        });
      });
      
      jQuery('#upload-main-preview').click(function() {           
            tb_show('', "<?php echo admin_url('media-upload.php?type=image&TB_iframe=1&width=640&height=551'); ?>");
            window.send_to_editor = function(html) {           
              var imgurl = jQuery('img',"<p>"+html+"</p>").attr('src');                     
              jQuery('#img').html("<img src='"+imgurl+"' style='max-width:100%'/><input type='hidden' name='file[preview]' value='"+imgurl+"' >");
              tb_remove();
              };
            return false;
        });
      </script>
</div>