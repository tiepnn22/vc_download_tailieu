<div class="uploadfrontend-section ">
    <div class="w3eden">
        <?php 
        if (is_user_logged_in()) { 
            if(isset($_SESSION['notify_upload'])) {
                $value = $_SESSION['notify_upload'];
                unset($_SESSION["notify_upload"]);
        ?>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-xl-12">
                        <div class="alert alert-success" role="alert"><?php echo $value; ?></div>
                    </div>
                </div>
        <?php
            }
        ?>
        <div class="row">
            <div class="col-12 col-sm-6 col-md-6 col-xl-6">
                <?php 
                    $description = get_field('mo_ta_khac', $post->ID);
                    echo $description;
                ?>
            </div>
            <div class="col-12 col-sm-6 col-md-6 col-xl-6">
            <?php include(wpdm_tpl_path('wpdm-add-new-file-front.php')); ?>
            </div>
        </div>
        <?php 
        } else {
            include(wpdm_tpl_path('wpdm-be-member.php'));
        }
        ?>
    </div>
</div>