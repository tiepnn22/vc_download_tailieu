<?php
if (!defined('ABSPATH')) die();

$settings = get_option('_wpdmpp_settings');
$currency_sign = wpdmpp_currency_sign();
$guest_checkout = (isset($settings['guest_checkout']) && $settings['guest_checkout'] == 1) ? 1 : 0;
$login_required = !is_user_logged_in() && $guest_checkout == 0?true:false;
$T = new \WPDM\Template();


if (is_array($cart_data) && count($cart_data) > 0) {
    ?>
    <div class="w3eden">
        <form method="post" class="abc" action="" name="checkout_cart_form">
            <input name="wpdmpp_update_cart" value="1" type="hidden">
            <table class="wpdm_cart table">
                <thead>
                <tr class="cart_header">
                    <th style="width:20px !important"></th>
                    <th><?php _e("Item", "wpdm-premium-package"); ?></th>
                    <th><?php _e("Unit Price", "wpdm-premium-package"); ?></th>
                    <th><?php _e("Role Discount", "wpdm-premium-package"); ?></th>
                    <th><?php _e("Coupon Code", "wpdm-premium-package"); ?></th>
                    <th><?php _e("Quantity", "wpdm-premium-package"); ?></th>
                    <th class="amt"><?php _e("Total", "wpdm-premium-package"); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php

                foreach ($cart_data as $ID => $item) {

                    $cart_item_info = "";
                    $cart_item_info = apply_filters("wpdmpp_cart_item_info", $cart_item_info, $item['ID']);
                    $thumb = wpdm_thumb($item['ID'], array(50, 50), false);
                    $variations = isset($item['variations']) ? "<small><i>" . implode(", ", $item['variations']) . "</i></small>" : '';

                    if (isset($item['coupon_amount'])) {
                        $discount_amount = $item['coupon_amount'];
                        $discount_style = "style='color:#008000; text-decoration:underline;'";
                        $discount_title = 'Discounted $' . $discount_amount . " for coupon code '{$item['coupon']}'";

                    } else {
                        $discount_amount = "";
                        $discount_style = "";
                        $discount_title = "";
                    }

                    if (isset($item['error']) && $item['error'] != '') {
                        $coupon_style = "border:1px solid #ff0000;";
                        $title = $item['error'];
                    } else {
                        $coupon_style = "";
                        $title = "";
                    }

                    $item['coupon'] = isset($item['coupon']) ? $item['coupon'] : '';
                    $item['coupon_amount'] = isset($item['coupon_amount']) ? $item['coupon_amount'] : 0;
                    $item_total = number_format((($item['price'] + $item['prices']) * $item['quantity']) - $item['coupon_amount'] - $item['discount_amount'], 2, ".", "");

                    ?>
                    <tr id='cart_item_<?php echo $ID; ?>'>
                        <td class='hidden-xs'>
                            <a class='wpdmpp_cart_delete_item btn btn-xs btn-danger' href='#'
                               onclick='return wpdmpp_pp_remove_cart_item(<?php echo $ID; ?>)'><i
                                    class='fa fa-trash-o'></i></a>
                        </td>
                        <td class='cart_item_title'>
                            <a class='wpdmpp_cart_delete_item btn btn-xs btn-danger visible-xs' href='#' onclick='return wpdmpp_pp_remove_cart_item(<?php echo $ID; ?>)'>
                                <i class='fa fa-trash-o'></i> <?php _e("Remove From Cart", "wpdm-premium-package"); ?>
                            </a>
                            <div class='pull-left thumb'><?php echo $thumb; ?></div>
                            <a target=_blank href='<?php echo get_permalink($ID); ?>'><?php echo $item['post_title']; ?></a><br>
                            <?php echo $variations . ' ' . $cart_item_info; ?>
                            <div class='clear'></div>
                        </td>
                        <td class='cart_item_unit_price' <?php echo $discount_style; ?> >
                            <span class='visible-xs'><?php echo __("Unit Price", "wpdm-premium-package"); ?>: </span>
                            <span class='ttip' title='<?php echo $discount_title; ?>'><?php echo $currency_sign . number_format($item['price'], 2, ".", ""); ?></span>
                        </td>
                        <td class=''>
                            <span class='visible-xs'><?php echo __("Role Discount", "wpdm-premium-package"); ?>
                                : </span>
                            <?php echo $currency_sign . number_format($item['discount_amount'], 2, '.', ''); ?>
                        </td>
                        <td>
                            <span class='visible-xs'><?php echo __("Coupon Code", "wpdm-premium-package"); ?>: </span>
                            <input style='width:100px;<?php echo $coupon_style; ?>' title='<?php echo $title; ?>'
                                   type='text' name='cart_items[<?php echo $ID; ?>][coupon]' value='<?php echo $item['coupon']; ?>' id='<?php echo $ID; ?>' class='ttip input-sm form-control' size=3/>
                        </td>
                        <td class='cart_item_quantity'>
                            <span class='visible-xs'><?php echo __("Quantity", "wpdm-premium-package"); ?>: </span>
                            <input type='number' style='width:60px' min='1' name='cart_items[<?php echo $ID; ?>][quantity]' value='<?php echo $item['quantity']; ?>' size=3 class=' input-sm form-control'/>
                        </td>
                        <td class='cart_item_subtotal amt'>
                            <span class='visible-xs'><?php echo __("Item Total", "wpdm-premium-package"); ?>: </span>
                            <?php echo $currency_sign . $item_total; ?>
                        </td>
                    </tr>
                    <?php

                }

                do_action('wpdmpp_cart_extra_row', $cart_data);
                $cart_coupon_discount = 0;
                $cart_subtotal = number_format((double)str_replace(',', '', wpdmpp_get_cart_subtotal()), 2);
                $cart_total = number_format((double)str_replace(',', '', wpdmpp_get_cart_total()), 2);
                $cart_tax = number_format((double)str_replace(',', '', wpmpp_get_cart_tax()), 2);
                $cart_total_with_tax = number_format((double)str_replace(',', '', $cart_total + $cart_tax), 2);
                $cart_coupon = wpdmpp_get_cart_coupon();
                $cart_coupon_discount = number_format($cart_coupon['discount'],2, '.', '');
                ?>

                <tr id="cart-total">
                    <td colspan="6" class="text-right hidden-xs"
                        align="right"><?php echo __("Subtotal", "wpdm-premium-package"); ?>:
                    </td>
                    <td class="amt" id="wpdmpp_cart_total">
                        <span class="visible-xs"><?php echo __("Subtotal", "wpdm-premium-package"); ?>: </span>
                        <strong><?php echo $currency_sign . $cart_subtotal; ?></strong>
                    </td>
                </tr>
                <tr id="cart-total">
                    <td colspan="4" class="text-right hidden-xs">
                        <div class="input-group input-group-sm" style="max-width: 220px">
                            <input type="text" name="coupon_code" class="form-control" value="<?php echo $cart_coupon['code']; ?>" placeholder="Coupon Code">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">Apply</button>
                            </span>
                        </div>
                    </td>
                    <td colspan="2" class="text-right hidden-xs"
                        align="right"><?php echo __("Coupon Discount", "wpdm-premium-package"); ?>:
                    </td>
                    <td class="amt" id="wpdmpp_cart_total">
                        <span class="visible-xs"><?php echo __("Coupon Discount", "wpdm-premium-package"); ?>: </span>
                        <?php echo $currency_sign . $cart_coupon_discount; ?>
                    </td>
                </tr>
                <?php if (wpdmpp_tax_active()) { ?>
                    <tr id="cart-tax">
                        <td colspan="6" class="text-right hidden-xs" align="right">
                            <?php echo __("Tax", "wpdm-premium-package"); ?>:
                        </td>
                        <td class="amt" id="wpdmpp_cart_tax">
                            <span class="visible-xs"><?php echo __("Tax", "wpdm-premium-package"); ?>: </span>
                            <?php echo $currency_sign . $cart_tax; ?></td>
                    </tr>
                <?php } ?>
                    <tr id="cart-total-with-tax">
                        <td colspan="6" class="text-right hidden-xs"
                            align="right"><?php echo __("Total", "wpdm-premium-package"); ?>:
                        </td>
                        <td class="amt" id="wpdmpp_cart_tax_total">
                            <strong><span class="visible-xs"><?php echo __("Total", "wpdm-premium-package"); ?>: </span>
                            <?php echo $currency_sign . $cart_total_with_tax; ?></strong>
                        </td>
                    </tr>
                <tr>
                    <td colspan="4">
                        <button type="button" class="btn btn-info " onclick="location.href='<?php echo $settings['continue_shopping_url']; ?>'">
                            <i class="fa fa-white fa-long-arrow-left"></i>&nbsp;<?php echo __("Continue Shopping", "wpdm-premium-package"); ?>
                        </button>
                        <button id="save-cart" type="button" class="btn btn-default">
                            <i class="fa fa-save"></i>&nbsp;<?php echo __("Save Cart", "wpdm-premium-package"); ?>
                        </button>
                    </td>
                    <td colspan="3" class="text-right" align="right">
                        <button class="btn btn-primary" type="button" onclick="document.checkout_cart_form.submit();">
                            <i class="fa fa-white fa-check"></i> <?php echo __("Update Cart", "wpdm-premium-package"); ?>
                        </button>
                    </td>
                </tr>
                </tbody>
            </table>

        </form>
        <div id="wpdm-after-cart"></div>


        <div id="wpdm-checkout">
        <?php
        if($login_required){
            echo $T->fetch('checkout-login.php', WPDMPP_BASE_DIR.'templates');
        } else {
            echo $T->fetch('payment-method.php', WPDMPP_BASE_DIR.'templates');
        }?>
        </div>

    </div>

    <?php

} else { ?>

    <div class="w3eden">
    <div class='panel panel-default'>
        <div class='panel-body text-danger'>
            <?php echo __("No item in cart.", "wpdm-premium-package") ; ?>
        </div>
        <div class='panel-footer text-right'>
            <a class='btn btn-sm btn-primary' href='<?php echo $settings['continue_shopping_url']; ?>'><?php echo __("Continue Shopping", "wpdm-premium-package"); ?> &nbsp;<i class='fa fa-long-arrow-right'></i></a>
        </div>
    </div>
    </div>


<?php }
