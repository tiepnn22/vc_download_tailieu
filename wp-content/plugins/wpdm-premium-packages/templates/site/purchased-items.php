<?php $sap = (isset($params['flaturl']) && $params['flaturl'] == 0)?"?udb_page=":""; ?>
<div class="panel panel-default dashboard-panel">
    <div class="panel-heading">
        <a href="<?php the_permalink();echo $sap; ?>purchases/orders/" class="pull-right"><?php _e('All Orders', 'wpdm-premium-package'); ?></a><?php _e('Purchased Items', 'wpdm-premium-package'); ?>
    </div>
    <table class="table">
        <thead>
            <tr>
                <th><?php _e('Product Name','wpdm-premium-package'); ?></th>
                <th><?php _e('Price','wpdm-premium-package'); ?></th>
                <th><?php _e('Order ID','wpdm-premium-package'); ?></th>
                <th><?php _e('Purchase Date','wpdm-premium-package'); ?></th>
                <th><?php _e('Download','wpdm-premium-package'); ?></th>
            </tr>
        </thead>
        <tbody>
        <?php
        foreach($purchased_items as $item){
            ?>

        <tr>
            <td><?php $title = get_the_title($item->pid); echo $title ? $title : '<span class="text-danger"><i class="fa fa-warning"></i> '.__('Product Deleted','wpdm-premium-package').'</span>'; ?></td>
            <td><?php echo wpdmpp_currency_sign().number_format($item->price,2); ?></td>
            <td><a href="<?php the_permalink(); echo $sap; ?>purchases/order/<?php echo $item->oid; ?>/"><?php echo $item->oid; ?></a></td>
            <td><?php echo date(get_option('date_format'),$item->odate); ?></td>
            <td>
                <?php if($item->order_status == 'Completed'){ ?>
                    <a href="<?php the_permalink();echo $sap; ?>purchases/order/<?php echo $item->oid; ?>/" class="btn btn-xs btn-primary btn-block"><?php _e('Download','wpdm-premium-package'); ?></a>
                <?php } else { ?>
                    <a href="<?php the_permalink();echo $sap; ?>purchases/order/<?php echo $item->oid; ?>/" class="btn btn-xs btn-danger btn-block"><?php _e('Expired','wpdm-premium-package'); ?></a>
                <?php } ?>
            </td>
        </tr>

        <?php } ?>
        </tbody>
    </table>
    <div class="panel-footer">
        <?php _e('If you are not seeing your purchased item:','wpdm-premium-package'); ?> <a class="btn btn-warning btn-xs" style="color: #ffffff !important;" href="<?php the_permalink(); ?>?udb_page=purchases/orders/"><?php _e('Fix It Here','wpdm-premium-package'); ?></a>
    </div>
</div>

