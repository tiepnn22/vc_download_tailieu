=== WPDM - Premium Packages ===
Contributors: codename065, shahriar0822
Donate link: 
Tags: shopping cart, digital store, online shop, wordpress ecommerce, wp eshop, products, product management, sell digital product, membership management, wordpress deals plugin
Requires at least: 3.5
Tested up to: 4.9
 
WPDM - Premium Packages is a free full-featured Shopping Cart / eCommerce Plugin with everything you need on an easy UI, to build eShop or marketplace.


== Changelog ==

= 3.8.5 =
* Added a new option to add global coupon code
* Added a separate page to manage all coupons

= 3.8.4 =
* Added new option for coupon code expire date
* Improved free download option for premium packages

= 3.8.3 =
* Fixed issue with installation function

= 3.8.1 =
* Fixed issue with role discount
* Fixed issue with individual file download for purchased packages

= 3.5.8 =
* 100% Translation ready
* Fixed a bug that shows wrong Net Subtotal of License variation in Invoice
* Fixed a bug that shows wrong Customer Billing Address in Invoice
* Added Premium Package template tags in Dashboard >> Downloads >> Template Editor screen
* Fixed currecy sign of total sales in admin orders screen

= 3.5.7 =
* Fixed issue with Save Cart feature in cart page
* Fixed Frontend uploader metabox issue ( Not loading Premium Package Data like price, variations etc )

= 3.5.6 =
* Fixed issue decimal point in order total
* Fixed issue with country dropdown in billing address
* Fixed issue with default payment method integration