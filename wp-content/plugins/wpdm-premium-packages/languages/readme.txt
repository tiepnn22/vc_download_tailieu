=== WPDM - Premium Package I18n ===

How to make translation file:

1. Open wpdm-premium-package.pot template file in Poedit ( https://poedit.net/download )

2. Create New Translation using the template file

3. Save new translation as wpdm-premium-package-LOCALE.po file. e.g. wpdm-premium-package-de_DE.po

Note: The LOCALE is the language code and/or country code of the site language setting under General Settings. For German language translation file name is wpdm-premium-package-de_DE.po